package org.ws4d.mobile.authenticator.client;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Random;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.authorization.AuthorizationException;
import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.dispatch.DefaultServiceReference;
import org.ws4d.java.dispatch.DeviceServiceRegistry;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_Helper_Android;
import org.ws4d.wscompactsecurity.authentication.engine.ActiveAuthentication;

import android.util.Base64;

public class Client extends DefaultClient {

	/* test Operation client logic
	 * 
	 * if test operation is invoked and a URI is supplied as parameter, the URI
	 * is interpreted as a light bulb's epr and the bulb is toggled
	 * 
	 * The return value is the bulb's new state
	 * 
	 */
	public static String testOperationToggleLightBulb(String deviceURI) {

		String rString;

		if (!JMEDSFramework.isRunning()) {
			JMEDSFramework.start(null);
		}

		EndpointReference epr = new EndpointReference(new AttributedURI(deviceURI));
		DeviceReference devRef = DeviceServiceRegistry.getDeviceReference(epr, SecurityKey.EMPTY_KEY, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		try {
			Device dev = devRef.getDevice();
			DefaultServiceReference sRef = (DefaultServiceReference) dev.getServiceReference(new URI("http://www.ws4d.org/LightBulbService"), SecurityKey.EMPTY_KEY);

			Service lightBulbService = sRef.getService();

			Operation statusOperation = lightBulbService.getOperation(null, "LightBulbState", null, null);
			Operation switchOperation = lightBulbService.getOperation(null, "LightBulbSwitch", null, null);

			ParameterValue stateIn = statusOperation.createInputValue();
			ParameterValue switchIn = switchOperation.createInputValue();

			ParameterValue stateResult = statusOperation.invoke(stateIn, CredentialInfo.EMPTY_CREDENTIAL_INFO);

			String oldState = ParameterValueManagement.getString(stateResult, "state");

			if (oldState.equals("on")) {
				ParameterValueManagement.setString(switchIn, "state", "off");
			} else {
				ParameterValueManagement.setString(switchIn, "state", "on");
			}

			ParameterValue switchResult = switchOperation.invoke(switchIn, CredentialInfo.EMPTY_CREDENTIAL_INFO);

			rString = ParameterValueManagement.getString(switchResult, "state");

		} catch (CommunicationException e) {
			e.printStackTrace();
			rString = deviceURI + " seems not to be a suitable lightbulb";
		} catch (AuthorizationException e) {
			e.printStackTrace();
			rString = "Don't know... Not allowed to toggle...";
		} catch (InvocationException e) {
			e.printStackTrace();
			rString = "Don't know... something went terribly wrong :(";
		}

		return rString;

	}

	private static ECC_DH_Helper_Android cryptoHelper = new ECC_DH_Helper_Android();

	public static ActiveAuthentication authenticateECCDH(ActiveAuthentication aa, boolean fake) {

		if (fake) { //TODO: Consider removing fake device parts from code - might be waaaayyyy outdated anyways...

			if (aa.getCurrentStage() == 0) {

				try {
					cryptoHelper.init();
					cryptoHelper.getParams().setOrigin(aa.getTarget());
					cryptoHelper.getParams().setTarget(aa.getOrigin());
					cryptoHelper.getParams().addOtherA(aa.getOriginMechanism());
					cryptoHelper.getParams().addOtherB(aa.getOriginMechanism());

					Random r = new Random(System.currentTimeMillis()); /* some seed - maybe use something more secure... */
					boolean[] oobSharedSecret = new boolean[20];
					for (int i=0; i<20; i++) {
						oobSharedSecret[i] = r.nextBoolean();
					}

					cryptoHelper.getParams().setOOBSharedSecret(oobSharedSecret);
					cryptoHelper.OOBkey2Integer();

					aa.setOOBSharedSecret(cryptoHelper.getParams().getOOBSharedSecretAsInt());

					cryptoHelper.createNonce();
					cryptoHelper.encryptPublicKey();

				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchProviderException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvalidAlgorithmParameterException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				aa.setCurveName(cryptoHelper.getParams().getCurveName());
				aa.setTargetNonce(Base64.encodeToString(cryptoHelper.getParams().getNonceA(), Base64.DEFAULT));
				aa.getTargetOther().add(WSSecurityForDevicesConstants.PinAuthentication);
				aa.setTargetPublic(Base64.encodeToString(cryptoHelper.getParams().getKeyPair().getPublic().getEncoded(), Base64.DEFAULT));
			} else if (aa.getCurrentStage() == 1) {
				byte[] encodedPublicKey = Base64.decode(aa.getOriginPublic(), Base64.DEFAULT);
				cryptoHelper.getParams().setForeignPublicKey(cryptoHelper.decodePublicKey(encodedPublicKey));
				cryptoHelper.getParams().setForeignCMAC(Base64.decode(aa.getOldOriginCMAC(), Base64.DEFAULT));
				cryptoHelper.getParams().setNonceB(Base64.decode(aa.getOriginNonce(), Base64.DEFAULT));

				try {
					cryptoHelper.calculateSharedSecret();
				} catch (InvalidKeyException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchProviderException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				cryptoHelper.calculateCMAC();

				aa.setOldTargetCMAC(Base64.encodeToString(cryptoHelper.getParams().getCMAC(), Base64.DEFAULT));

				if (!cryptoHelper.checkCMAC()) {
					aa.setErrorOccured(true);
					aa.setErrorMessage("Origin's CMAC Test Failed!!");
				}

				cryptoHelper.calculateMasterKey();

				String key = "";

				for (int i = 0; i < cryptoHelper.getParams().getMasterKey().length; i++) {
					key += Integer.toHexString((cryptoHelper.getParams().getMasterKey()[i]) & 0x00FF) + " ";
				}

				aa.setErrorMessage(aa.getErrorMessage() + " \n\n Calculated Master Key: " + key);

			} else {
				/* SRSLY!! WAT?! */
			}


			aa.setCurrentStage(aa.getCurrentStage()+1);

		} else {

			if (!JMEDSFramework.isRunning()) {
				JMEDSFramework.start(null);
			}

			EndpointReference targetEpr = null;
			if (aa.getIntermediary() == null) {
				targetEpr = new EndpointReference(new AttributedURI(aa.getTarget()));
			} else {
				targetEpr = new EndpointReference(new AttributedURI(aa.getIntermediary()));
			}
			DeviceReference targetDeviceReference = DeviceServiceRegistry.getDeviceReference(targetEpr, SecurityKey.EMPTY_KEY, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
			Device targetDevice = null;
			try {
				targetDevice = targetDeviceReference.getDevice();
			} catch (CommunicationException e) {
				e.printStackTrace();
				aa.setErrorOccured(true);
				aa.setErrorMessage("Could not find target device! Exception:" + e.getMessage());
				return aa;
			}

			DefaultServiceReference targetAuthenticationServiceReference = (DefaultServiceReference) (targetDevice.getServiceReferences(new QNameSet(QName.construct(WSSecurityForDevicesConstants.STSAuthenticationECCDHBindingPortType)), SecurityKey.EMPTY_KEY)).next();
			if (targetAuthenticationServiceReference == null) {
				aa.setErrorOccured(true);
				aa.setErrorMessage("Target Device does not seem to host an authentication service");
				return aa;
			}

			Service targetAuthenticationService = null;
			try {
				targetAuthenticationService = targetAuthenticationServiceReference.getService();
			} catch (CommunicationException e) {
				e.printStackTrace();
				aa.setErrorOccured(true);
				aa.setErrorMessage("Could not find target authentication service! Exception:" + e.getMessage());
				return aa;
			}

			if (aa.getCurrentStage() == 0) {

				Operation op = targetAuthenticationService.getOperation(
						QName.construct(WSSecurityForDevicesConstants.STSAuthenticationECCDHBindingPortType),
						WSSecurityForDevicesConstants.STSAuthenticationECDH1MethodName,
						Constants.WST_ISSUE_BINDING_RST_ISSUE_ACTION,
						Constants.WST_ISSUE_BINDING_RSTR_ISSUE_ACTION);

				if (op == null) {
					aa.setErrorOccured(true);
					aa.setErrorMessage("Could not find Operation " + WSSecurityForDevicesConstants.STSAuthenticationECDH1MethodName);
					return aa;
				}

				ParameterValue param = op.createInputValue();

				ParameterValueManagement.setString(param, Constants.TokenType.getLocalPart(), WSSecurityForDevicesConstants.SymmetricTokenType);
				ParameterValueManagement.setString(param, Constants.RequestType.getLocalPart(), WSSecurityForDevicesConstants.ECCDH1RequestType);
				ParameterValueManagement.setString(param, Constants.AppliesTo.getLocalPart(), aa.getTarget());
				ParameterValueManagement.setString(param, Constants.OnBehalfOf.getLocalPart(), aa.getOrigin());
				ParameterValueManagement.setString(param, Constants.AuthenticationType.getLocalPart(), aa.getTargetMechanism());
				if (aa.isBroker()) {
					ParameterValueManagement.setString(param, Constants.isBroker.getLocalPart(), "true");
					if (aa.getIsAuthorizer()) {
						ParameterValueManagement.setAttributeValue(param, Constants.isBroker.getLocalPart(), Constants.authorizerAttribute, "true");
					}
				}
				if (aa.getSelectedSecurityAlgorithmsSet().getSignatureAlgorithm() != null)
					ParameterValueManagement.setString(param, Constants.SignWith.getLocalPart(), aa.getSelectedSecurityAlgorithmsSet().getSignatureAlgorithm().toString());
				if (aa.getSelectedSecurityAlgorithmsSet().getEncryptionAlgorithm() != null)
					ParameterValueManagement.setString(param, Constants.EncryptWith.getLocalPart(), aa.getSelectedSecurityAlgorithmsSet().getEncryptionAlgorithm().toString());
				//				if (aa.getSelectedSecurityAlgorithmsSet().getDerivationAlgorithm() != null)
				//					ParameterValueManagement.setString(param, "?????????", aa.getSelectedSecurityAlgorithmsSet().getDerivationAlgorithm().toString());

				ParameterValue response = null;

				try {
					Log.info("Invoking with" + param.toString());
					response = op.invoke(param, CredentialInfo.EMPTY_CREDENTIAL_INFO);
					Log.info("Received" + response.toString());
				} catch (AuthorizationException e) {
					e.printStackTrace();
					aa.setErrorOccured(true);
					aa.setErrorMessage("Could not invoke Operation! Exception: " + e.getMessage());
					return aa;
				} catch (InvocationException e) {
					e.printStackTrace();
					aa.setErrorOccured(true);
					aa.setErrorMessage("Could not invoke Operation! Exception: " + e.getMessage());
					return aa;
				} catch (CommunicationException e) {
					e.printStackTrace();
					aa.setErrorOccured(true);
					aa.setErrorMessage("Could not invoke Operation! Exception: " + e.getMessage());
					return aa;
				}

				aa.setCurveName(ParameterValueManagement.getString(response, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_curvename.getLocalPart()));
				aa.setTargetNonce(ParameterValueManagement.getString(response, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_nonce.getLocalPart()));
				aa.getTargetOther().add(ParameterValueManagement.getString(response, Constants.AuthenticationType.getLocalPart()));
				aa.setTargetPublic(ParameterValueManagement.getString(response, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_public_key.getLocalPart()));

				String spin = ParameterValueManagement.getString(response, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_numerical_pin.getLocalPart());
				if (spin != null)
					aa.setOOBSharedSecret(Integer.parseInt(spin));

			} else if (aa.getCurrentStage() == 1) {
				Operation op = targetAuthenticationService.getOperation(
						QName.construct(WSSecurityForDevicesConstants.STSAuthenticationECCDHBindingPortType),
						WSSecurityForDevicesConstants.STSAuthenticationECDH2MethodName,
						Constants.WST_ISSUE_BINDING_RSTR_ISSUE_ACTION,
						Constants.WST_ISSUE_BINDING_RSTRC_ISSUEFINAL_ACTION);
				if (op == null) {
					aa.setErrorOccured(true);
					aa.setErrorMessage("Could not find Operation " + WSSecurityForDevicesConstants.STSAuthenticationECDH2MethodName);
					return aa;
				}

				ParameterValue param = op.createInputValue();

				ParameterValueManagement.setString(param, Constants.TokenType.getLocalPart(), WSSecurityForDevicesConstants.SymmetricTokenType);
				ParameterValueManagement.setString(param, Constants.RequestType.getLocalPart(), WSSecurityForDevicesConstants.ECCDH2RequestType);
				ParameterValueManagement.setString(param, Constants.AppliesTo.getLocalPart(), aa.getTarget());
				ParameterValueManagement.setString(param, Constants.OnBehalfOf.getLocalPart(), aa.getOrigin());
				ParameterValueManagement.setString(param, Constants.AuthenticationType.getLocalPart(), aa.getTargetMechanism());

				ParameterValueManagement.setString(param, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_public_key.getLocalPart(), aa.getOriginPublic());
				ParameterValueManagement.setString(param, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_cmac.getLocalPart(), aa.getNewOriginCMAC());
				ParameterValueManagement.setString(param, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_nonce.getLocalPart(), aa.getOriginNonce());

				ParameterValue response = null;
				try {
					Log.info("Invoking with" + param.toString());
					response = op.invoke(param, CredentialInfo.EMPTY_CREDENTIAL_INFO);
					Log.info("Received" + response.toString());
				} catch (AuthorizationException e) {
					e.printStackTrace();
					aa.setErrorOccured(true);
					aa.setErrorMessage("Could not invoke Operation! Exception: " + e.getMessage());
					return aa;
				} catch (InvocationException e) {
					e.printStackTrace();
					aa.setErrorOccured(true);
					aa.setErrorMessage("Could not invoke Operation! Exception: " + e.getMessage());
					return aa;
				} catch (CommunicationException e) {
					e.printStackTrace();
					aa.setErrorOccured(true);
					aa.setErrorMessage("Could not invoke Operation! Exception: " + e.getMessage());
					return aa;
				}

				aa.setOldTargetCMAC(           ParameterValueManagement.getString(response, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_cmac.getLocalPart()));
				aa.setTokenIdentification(     ParameterValueManagement.getString(response, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + Constants.SecurityContextToken.getLocalPart() + "/" + Constants.Identifier.getLocalPart()));
				aa.setTokenSignatureAlgorithm( ParameterValueManagement.getString(response, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + Constants.SecurityContextToken.getLocalPart() + "/" + Constants.SignWith.getLocalPart()));
				aa.setTokenEncryptionAlgorithm(ParameterValueManagement.getString(response, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + Constants.SecurityContextToken.getLocalPart() + "/" + Constants.EncryptWith.getLocalPart()));
				String sBroker = ParameterValueManagement.getString(response, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.isBroker.getLocalPart());
				if (sBroker != null && sBroker.equals("true")) {
					aa.setTargetIsBroker(true);
				} else {
					aa.setTargetIsBroker(false);
				}
				String authrz = ParameterValueManagement.getAttributeValue(response, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.isBroker.getLocalPart(), Constants.authorizerAttribute);
				if (authrz != null && authrz.equals("true")) {
					aa.setTargetIsAuthorizer(true);
				} else {
					aa.setTargetIsAuthorizer(false);
				}

			} else {
				//			WAT?!?!!
			}
			aa.setCurrentStage(aa.getCurrentStage()+1);

		}

		return aa;
	}

	public Client() {
	}

}
