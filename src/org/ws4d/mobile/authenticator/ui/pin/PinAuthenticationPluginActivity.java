package org.ws4d.mobile.authenticator.ui.pin;

import org.ws4d.mobile.authenticator.R;
import org.ws4d.mobile.authenticator.ui.PluginActivity;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class PinAuthenticationPluginActivity extends PluginActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pin_authentication_plugin);

		boolean autostart = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean("settings_menu_pin_autostart", true);


		if (getActiveAuthentication() != null) {
			TextView tv = (TextView) findViewById(R.id.pinEditText);
			if (!isPrimary()) {
				tv.setText(Integer.toString(getActiveAuthentication().getOOBSharedSecret()));
				tv.setEnabled(false);
			}
		}

		if (getBundle() != null) {

			final Button launchButton = (Button) findViewById(R.id.pinLaunchButton);
			final Button okButton = (Button) findViewById(R.id.pinOKbutton);

			launchButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					launchButton.setEnabled(false);
					startAuthentication();
					Toast.makeText(getApplicationContext(), "Client request started. Pin should appear soon.", Toast.LENGTH_SHORT).show();
				}
			});

			setPostReqThread( new Runnable() {
				@Override
				public void run() {
					TextView tv = (TextView) findViewById(R.id.pinEditText);
					if (getActiveAuthentication().getErrorOccured()) {
						Toast.makeText(getApplicationContext(), "An Error occured!!\n\n" + getActiveAuthentication().getErrorMessage(), Toast.LENGTH_LONG).show();
					}
					if (getBundle().getBoolean("fake", false)) {
						tv.setText(Integer.valueOf(getActiveAuthentication().getOOBSharedSecret()).toString());
					}

					Button okButton = (Button) findViewById(R.id.pinOKbutton);
					okButton.setEnabled(true);

					if (getBundle().getBoolean("fake", false)) {
						Toast.makeText(getApplicationContext(), "You are in fake mode. Please simply hit OK to report the fake PIN", Toast.LENGTH_LONG).show();
					} else {
						/* focuses the textview and opens keypad */
						tv.requestFocus();
						InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.showSoftInput(tv, InputMethodManager.SHOW_IMPLICIT);
					}
				}
			}) ;

			boolean fake = getBundle().getBoolean("fake", false);

			if (fake) {
				launchButton.setEnabled(true);
				Toast.makeText(getApplicationContext(), "You are in Fake Mode. Simply Press Launch!", Toast.LENGTH_LONG).show();
			} else if (isPrimary() && !isFinal()) {
				launchButton.setEnabled(true);
			} else {
				okButton.setEnabled(true);
			}

			okButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					TextView tv = (TextView) findViewById(R.id.pinEditText);
					reportPinAndFinish(Integer.parseInt(tv.getText().toString()));
				}
			});

			if (autostart && !fake && isPrimary() && !isFinal()) {
				launchButton.setEnabled(false);
				startAuthentication();
				Toast.makeText(getApplicationContext(), "Client request started. Pin should appear soon.", Toast.LENGTH_SHORT).show();
			}

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pin_authentication_plugin, menu);
		return true;
	}
}
