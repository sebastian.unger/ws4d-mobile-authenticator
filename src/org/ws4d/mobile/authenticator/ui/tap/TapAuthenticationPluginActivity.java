package org.ws4d.mobile.authenticator.ui.tap;

import org.ws4d.java.util.Log;
import org.ws4d.mobile.authenticator.R;
import org.ws4d.mobile.authenticator.ui.PluginActivity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class TapAuthenticationPluginActivity extends PluginActivity {

	ImageView switchViewCurrent = null;
	ImageView switchViewNext1 = null;
	ImageView switchViewNext2 = null;
	ImageView switchViewNext3 = null;
	TextView integerLabel = null;
	TextView binaryLabel = null;
	TextView keyLabel = null;
	Drawable switchOnImage = null;
	Drawable switchOffImage = null;
	Drawable readyImage = null;
	Drawable oneImage = null;
	Drawable twoImage = null;
	Drawable threeImage = null;
	Drawable transparentImage = null;
	Drawable currentImage = null;
	Drawable nextImage_1 = null;
	Drawable nextImage_2 = null;
	Drawable nextImage_3 = null;
	Drawable noSharedSecretImage = null;
	ProgressBar tapProgress = null;
	Button stopButton = null;
	Button startButton = null;
	SeekBar keyBar = null;

	boolean runningModeEnabled = false;  // indicates if we are currently displaying a pin
	boolean demoModeEnabled = false;
	Thread myThread = null;
	String bin_key = "0";
	int demoKey = 349525;
	int progressCount;
	final int symbol_time=1000;





	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tap_authentication_plugin);

		tapProgress = (ProgressBar) findViewById(R.id.tap_progressBar);
		integerLabel = (TextView) findViewById(R.id.tap_IntegerLabel);
		binaryLabel = (TextView) findViewById(R.id.tap_BinaryLabel);
		keyLabel = (TextView) findViewById(R.id.tap_textView_key);
		switchViewCurrent = (ImageView) findViewById(R.id.tap_imageView_switch);
		switchViewNext1 = (ImageView) findViewById(R.id.tap_imageView_switch_next1);
		switchViewNext2 = (ImageView) findViewById(R.id.tap_imageView_switch_next2);
		switchViewNext3 = (ImageView) findViewById(R.id.tap_imageView_switch_next3);
		switchOnImage = getResources().getDrawable(R.drawable.tap_switch_on);
		switchOffImage = getResources().getDrawable(R.drawable.tap_switch_off);
		readyImage = getResources().getDrawable(R.drawable.tap_ready);
		oneImage = getResources().getDrawable(R.drawable.tap_1);
		twoImage = getResources().getDrawable(R.drawable.tap_2);
		threeImage = getResources().getDrawable(R.drawable.tap_3);
		transparentImage = getResources().getDrawable(R.drawable.tap_transparent);
		noSharedSecretImage = getResources().getDrawable(R.drawable.tap_nosecret);
		stopButton = (Button) findViewById(R.id.tap_button_stop);
		startButton = (Button) findViewById(R.id.tap_button_start);


		// defined init state, so the count down doesn't disappear, when startbit is pushed in
		currentImage=readyImage;
		nextImage_1 = threeImage;
		nextImage_2 = twoImage;
		nextImage_3 = oneImage;


		keyBar = (SeekBar) findViewById(R.id.tap_seekBar_key);
		keyBar.setProgress(demoKey);
		keyBar.setOnSeekBarChangeListener(seekBarChangeListener);
		stopButton.setEnabled(false);
		enableDemoModeUi(true);

		if (getActiveAuthentication() != null && !isPrimary()) {
			enableDemoModeUi(false);
		} else {
			enableDemoModeUi(true);
			Toast.makeText(getApplicationContext(), "You are in Demo Mode", Toast.LENGTH_LONG).show();
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		/* stop thread when app gets closed */
		if (myThread != null) {
			myThread.interrupt();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.tap_authentication_plugin, menu);
		return true;
	}

	public void setImage(Drawable image) {
		currentImage=image;
		runOnUiThread(uiUpdateCurrentImage);
		return;
	}


	/************************************************
	 *      	 Main Functionality					*
	 ************************************************/

	Thread threatStartTapFlickering = new Thread() {

		@Override
		public void run() {

			if(demoModeEnabled){
				bin_key=Integer.toBinaryString(demoKey);
			} else {
				bin_key=Integer.toBinaryString(getActiveAuthentication().getOOBSharedSecret());
			}


			Log.info("hannes - " + bin_key);
			bin_key=formatMitNullenLinks(bin_key, 20);
			Log.info("hannes - " + bin_key);


			runningModeEnabled = true;
			runOnUiThread(uiUpdateRunningModeUi);

			// instantly display the count down
			resetImageArray();


			if(runningModeEnabled){pushImageIntoArrayAndWait(switchOnImage, symbol_time);}     // startbit

			for(progressCount=1;progressCount<=20;progressCount++){
				Log.debug("hannes - Count: " + Integer.toString(progressCount));
				runOnUiThread(uiUpdateProgessbar);
				if(runningModeEnabled){

					if(bin_key.charAt(progressCount-1)=='1'){
						pushImageIntoArrayAndWait(switchOnImage, symbol_time);
					}else{
						pushImageIntoArrayAndWait(switchOffImage, symbol_time);
					};
				};
			};


			// flush the last 3 images

			for(int temp=1;temp<=3;temp++){
				progressCount++;
				runOnUiThread(uiUpdateProgessbar);
				if(runningModeEnabled){pushImageIntoArrayAndWait(transparentImage, symbol_time);};
			}

			// bring back count-down
			resetImageArray();


			progressCount = 0;
			runOnUiThread(uiUpdateProgessbar);

			runningModeEnabled = false;
			runOnUiThread(uiUpdateRunningModeUi);
			myThread = null;

		}
	};




	public String formatMitNullenLinks(String value, int len) {
		String result = String.valueOf(value);
		while (result.length() < len) {
			result = "0" + result;
		}
		return result;
	}


	/************************************************
	 *      			 Buttons					*
	 ************************************************/

	public void onButtonStartClick(View v) {
		Log.debug("hannes - Button 1 Clicked");
		if (myThread == null) { /* This way, it won't be started more that once */
			myThread = new Thread(threatStartTapFlickering);
			myThread.start();
		}
	}

	public void onButtonStopClick(View v) {
		Log.debug("hannes - Button 2 Clicked");
		runningModeEnabled = false;
		// bring back the count down (cosmetic)
		runOnUiThread(uiUpdateRunningModeUi);
	}


	/************************************************
	 *      			 Images						*
	 ************************************************/

	Runnable uiUpdateCurrentImage = new Runnable() {     	// instantly updates FIRST Image on screen
		@Override
		public void run() {
			//			iv.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
			switchViewCurrent.setImageDrawable(currentImage);
		}
	};


	Runnable uiUpdateImageArray = new Runnable() {			// instantly updates ALL Images on screen
		@Override
		public void run() {
			//			iv.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
			switchViewCurrent.setImageDrawable(currentImage);
			switchViewNext1.setImageDrawable(nextImage_1);
			switchViewNext2.setImageDrawable(nextImage_2);
			switchViewNext3.setImageDrawable(nextImage_3);

		}
	};


	public void resetImageArray() {  // sets ready,3,2,1 and instantly updates screen
		currentImage=readyImage;
		nextImage_1 = threeImage;
		nextImage_2 = twoImage;
		nextImage_3 = oneImage;
		runOnUiThread(uiUpdateImageArray);
		return;
	}



	public void pushImageIntoArray(Drawable image) {		// push + INSTANT update
		currentImage=nextImage_1;
		nextImage_1=nextImage_2;
		nextImage_2=nextImage_3;
		nextImage_3=image;
		runOnUiThread(uiUpdateImageArray);
		return;
	}

	public void pushImageIntoArrayAndWait(Drawable image, int time) {  // push + DELAYED update
		currentImage=nextImage_1;
		nextImage_1=nextImage_2;
		nextImage_2=nextImage_3;
		nextImage_3=image;
		runOnUiThread(uiUpdateImageArray);
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			return;
		}
		return;
	}



	/************************************************
	 * 			 Modes and Progress Bar				*
	 ************************************************/


	Runnable uiUpdateRunningModeUi  = new Runnable() {

		@Override
		public void run() {
			startButton.setEnabled(!runningModeEnabled);
			stopButton.setEnabled(runningModeEnabled);
			keyBar.setEnabled(!runningModeEnabled);
		}
	};

	Runnable uiUpdateProgessbar  = new Runnable() {					// -4 because we don't count the count down itself!
		@Override
		public void run() {
			if(progressCount>=4){
				tapProgress.setProgress(progressCount-3);
			}else{
				tapProgress.setProgress(0);
			};
		};
	};


	OnSeekBarChangeListener seekBarChangeListener = new OnSeekBarChangeListener() {

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			/* nothing to do at the precise moment when you put your thumb on that seek bar */
		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			/* nothing to do at the precise moment when you release your thumb either... */
		}

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			demoKey = progress;
			integerLabel.setText("Integer: " + Integer.valueOf(demoKey).toString());
			bin_key=Integer.toBinaryString(demoKey);
			bin_key=formatMitNullenLinks(bin_key, 20);
			binaryLabel.setText("Binary: " + bin_key);

		}
	};


	public void enableDemoModeUi(boolean enabled){

		if(enabled){
			Log.info("hannes - Demomode enabled");

			demoModeEnabled = true;

			keyBar.setVisibility(View.VISIBLE);
			keyLabel.setVisibility(View.VISIBLE);
			binaryLabel.setVisibility(View.VISIBLE);

			demoKey = keyBar.getProgress();
			integerLabel.setText("Integer: " + Integer.valueOf(demoKey).toString());
			bin_key=Integer.toBinaryString(demoKey);
			bin_key=formatMitNullenLinks(bin_key, 20);
			binaryLabel.setText("Binary: " + bin_key);

			setImage(readyImage);
			startButton.setEnabled(true);
		}

		if(!enabled){
			Log.info("hannes - Demomode disabled");

			demoModeEnabled = false;

			keyBar.setVisibility(View.GONE);
			keyLabel.setVisibility(View.GONE);
			binaryLabel.setVisibility(View.GONE);

			if(getActiveAuthentication().getOOBSharedSecret()==-1){

				integerLabel.setText("Shared Key is: empty");
				setImage(noSharedSecretImage);
				startButton.setEnabled(false);

			}else{
				integerLabel.setText("Shared Key is: " + Integer.toString(getActiveAuthentication().getOOBSharedSecret()));
				setImage(readyImage);
				startButton.setEnabled(true);

			}
		}

	}

}


