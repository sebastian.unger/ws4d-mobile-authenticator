package org.ws4d.mobile.authenticator.ui;

import org.ws4d.mobile.authenticator.client.Client;
import org.ws4d.mobile.authenticator.device.DPWSDeviceService;
import org.ws4d.wscompactsecurity.authentication.engine.ActiveAuthentication;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

public class PluginActivity extends Activity {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mBundle= getIntent().getExtras();

		if (mBundle != null) {
			mActiveAuthentication = (ActiveAuthentication) mBundle.getSerializable("org.ws4d.authentication");
			setPrimary(mBundle.getBoolean("primary", true));
			setFinal(mBundle.getBoolean("final", false));
		}
	}


	private Bundle mBundle = null;
	private ActiveAuthentication mActiveAuthentication = null;
	private boolean primary = true; /* means client-side. So the one, where the pin is entered (not displayed) */
	private boolean isFinal = false; /* means it's not about bypassing anything. Most likely, it means a phone
										itself is about to authenticate with another entity */

	private Runnable postReqThread = null;

	/**
	 * This is what is to be executed on the UI Thread after Authentication took place
	 * @param r
	 */
	public void setPostReqThread (Runnable r) {
		postReqThread = r;
	}

	public Bundle getBundle() {
		return mBundle;
	}
	public void setBundle(Bundle bundle) {
		mBundle = bundle;
	}
	public ActiveAuthentication getActiveAuthentication() {
		return mActiveAuthentication;
	}
	public void setActiveAuthentication(ActiveAuthentication aa) {
		mActiveAuthentication = aa;
	}

	public boolean isPrimary() {
		return primary;
	}
	public void setPrimary(boolean primary) {
		this.primary = primary;
	}
	public boolean isFinal() {
		return isFinal;
	}
	public void setFinal(boolean isFinal) {
		this.isFinal = isFinal;
	}

	protected void startAuthentication() {
		Thread t = new Thread(reqThread);
		t.start();
	}

	private Runnable reqThread = new Runnable() {

		@Override
		public void run() {
			setActiveAuthentication( Client.authenticateECCDH(getActiveAuthentication(), getBundle().getBoolean("fake", false)) );
			/* report back Target Reponse to device logic, so Origin gets the reponse */

			if (mServiceBound) {

				Message msg = Message.obtain(null, DPWSDeviceService.MSG_AUTHENTICATION_CLIENT_REPORT_BACK);

				Bundle b = new Bundle();

				b.putSerializable("org.ws4d.authentication", getActiveAuthentication());

				msg.setData(b);

				try {
					mReportBackService.send(msg);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (postReqThread != null) {
				runOnUiThread(postReqThread);
			}
		}
	};

	Messenger mReportBackService = null;
	boolean mServiceBound = false;

	/* Binding to the service is necessary for reporting request results back to device logic */
	private ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mReportBackService = null;
			mServiceBound = false;
			Log.d("WS4D", "FlickerAuthenticationActivity: Unbound from Service");
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mReportBackService = new Messenger(service);
			mServiceBound = true;
			Log.d("WS4D", "FlickerAuthenticationActivity: Bound to Service");
		}
	};

	@Override
	protected void onStart() {
		super.onStart();
		bindService(new Intent(this, DPWSDeviceService.class), mConnection, Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (mServiceBound) {
			unbindService(mConnection);
			mServiceBound = false;
		}
	}

	protected void reportPinAndFinish(int pin) {
		if (isPrimary() && !isFinal()) {
			Intent i = new Intent(getApplicationContext(), AuthenticatorOverview.class);
			getActiveAuthentication().setOOBSharedSecret(pin);
			i.putExtra("org.ws4d.authentication", getActiveAuthentication());
			startActivity(i);
		} else if (isPrimary() && isFinal()) {
			Intent returnIntent = new Intent();
			returnIntent.putExtra("org.ws4d.intpin", pin);
			setResult(RESULT_OK, returnIntent);
		}
		finish();
	}

}
