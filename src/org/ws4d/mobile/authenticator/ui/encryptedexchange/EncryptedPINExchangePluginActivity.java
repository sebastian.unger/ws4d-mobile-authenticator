package org.ws4d.mobile.authenticator.ui.encryptedexchange;

import org.ws4d.mobile.authenticator.R;
import org.ws4d.mobile.authenticator.ui.PluginActivity;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

public class EncryptedPINExchangePluginActivity extends PluginActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_encrypted_pin_exchange_plugin);

		setPostReqThread(new Runnable() {

			@Override
			public void run() {
				if (getActiveAuthentication().getErrorOccured()) {
					Toast.makeText(getApplicationContext(), "An Error occured!!\n\n" + getActiveAuthentication().getErrorMessage(), Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(getApplicationContext(), "Received encrypted PIN (" + Integer.toString(getActiveAuthentication().getOOBSharedSecret()) + "). Proceed.", Toast.LENGTH_SHORT).show();
					reportPinAndFinish(getActiveAuthentication().getOOBSharedSecret());
				}
			}
		});

		ImageView iv = (ImageView) findViewById(R.id.kittenImageView);
		iv.setBackgroundResource(R.drawable.kittyanim);

		AnimationDrawable anim = (AnimationDrawable) iv.getBackground();
		anim.start();

		if (getActiveAuthentication() != null) {
			startAuthentication();
		}

	}
}
