package org.ws4d.mobile.authenticator.ui.trustrelationships;

import java.util.ArrayList;

import org.ws4d.mobile.authenticator.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class TrustRelationshipListAdapter extends ArrayAdapter<TrustRelationshipItem> {
	ArrayList<TrustRelationshipItem> mtrlist = null;
	Context ctx = null;

	public TrustRelationshipListAdapter(Context context, int resource, ArrayList<TrustRelationshipItem> list) {
		super(context, resource, list);
		ctx = context;
		mtrlist = list;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;

		if (v == null) {
			LayoutInflater vi = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.trust_relationship_list_item, null);
		}

		final TrustRelationshipItem trItem = mtrlist.get(position);

		if (trItem != null) {
			TextView nv = (TextView) v.findViewById(R.id.trust_relationship_name);
			TextView dv = (TextView) v.findViewById(R.id.trust_relationship_detail);

			if (trItem.getName() != null)
				nv.setText(trItem.getName());
			if (trItem.getDetail() != null)
				dv.setText(trItem.getDetail());
		}

		return v;
	}
}
