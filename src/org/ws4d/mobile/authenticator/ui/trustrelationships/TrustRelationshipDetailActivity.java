package org.ws4d.mobile.authenticator.ui.trustrelationships;

import org.ws4d.java.incubation.CredentialManagement.SecurityContext;
import org.ws4d.java.incubation.WSSecurityForDevices.AlgorithmConstants;
import org.ws4d.java.types.QName;
import org.ws4d.mobile.authenticator.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class TrustRelationshipDetailActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.trust_relationship_detail_layout);

		SecurityContext sct = (SecurityContext) getIntent().getExtras().getSerializable("context");

		if (sct != null) {
			TextView t1 = (TextView) findViewById(R.id.trust_relationship_detail_view_refrence_label);
			t1.setText(sct.getContextReference());

			TextView t2 = (TextView) findViewById(R.id.trust_relationship_detail_view_broker_label);
			t2.setText("Broker: " + (sct.isBroker() ? "yes" : "no"));

			TextView t3 = (TextView) findViewById(R.id.trust_relationship_detail_view_token_id_label);
			t3.setText(sct.getSecurityToken().getIdentification());

			TextView t4 = (TextView) findViewById(R.id.trust_relationship_detail_view_token_sig_label);
			t4.setText(((QName) AlgorithmConstants.algorithmString.get(sct.getSecurityToken().getsAlgorithmType())).toString());

			TextView t5 = (TextView) findViewById(R.id.trust_relationship_detail_view_token_enc_label);
			t5.setText(((QName) AlgorithmConstants.algorithmString.get(sct.getSecurityToken().geteAlgorithmType())).toString());

			TextView t6 = (TextView) findViewById(R.id.trust_relationship_detail_view_token_key_label);
			StringBuilder sb = new StringBuilder();
			byte[] key = sct.getSecurityToken().getKeymaterial();
			for (int i = 0; i < key.length; i++) {
				sb.append(String.format("%02X ", key[i]));
			}
			t6.setText(sb.toString());
		}

		Button b = (Button) findViewById(R.id.button1);
		b.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}
