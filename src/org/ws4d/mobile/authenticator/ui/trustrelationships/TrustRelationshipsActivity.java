package org.ws4d.mobile.authenticator.ui.trustrelationships;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.ws4d.java.incubation.CredentialManagement.SecurityContext;
import org.ws4d.java.incubation.CredentialManagement.SimpleContextDB;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.mobile.authenticator.R;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class TrustRelationshipsActivity extends Activity {

	TrustRelationshipListAdapter mListAdapter = null;
	ArrayList<TrustRelationshipItem> mList = null;

	ListView lv = null;
	Button b = null;
	ProgressBar pb = null;

	Toast hint = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_trust_relationships);

		hint = Toast.makeText(getApplicationContext(), "Longpress button to remove all contexts", Toast.LENGTH_LONG);

		mList = new ArrayList<TrustRelationshipItem>();
		mListAdapter = new TrustRelationshipListAdapter(getApplicationContext(), R.layout.trust_relationship_list_item, mList);

		lv = (ListView) findViewById(R.id.trust_relationship_list);
		lv.setAdapter(mListAdapter);
		lv.setOnItemClickListener(itemClick);
		registerForContextMenu(lv);

		b = (Button) findViewById(R.id.clear_all_button);
		b.setOnTouchListener(buttonTouchListener);

		pb = (ProgressBar) findViewById(R.id.clear_all_button_progress);

		pb.setMax(255);

		fillList();

	}

	private void fillList() {
		SimpleContextDB db = AuthenticationEngine.getInstance().getContextDatabase();
		HashMap contexts = db.getAllContexts();
		Iterator it = contexts.values().iterator();
		while (it.hasNext()) {
			SecurityContext sc = (SecurityContext) it.next();
			mList.add(new TrustRelationshipItem(sc.getContextReference(), sc.getSecurityToken() == null ? "" : sc.getSecurityToken().getIdentification()));
		}
		mListAdapter.notifyDataSetChanged();
	}

	/* **************************************** *
	 * Code below this point is for super fancy
	 * button that you need to press longer!
	 * **************************************** */
	Timer timer = null;
	private class myTimerTask extends TimerTask{
		@Override
		public void run() {
			runOnUiThread(incrementProgressbar);
		}
	}

	Runnable incrementProgressbar = new Runnable() {
		@Override
		public void run() {
			int color = Color.argb(pb.getProgress(), 0xFF, 0x00, 0x00);
			pb.getProgressDrawable().setColorFilter(color, Mode.SRC_IN);
			if (pb.getProgress() >= pb.getMax())
				progressBarFull();
			else {
				if (!complete)
					pb.incrementProgressBy(2);
			}
		}
	};

	boolean complete = false;

	void progressBarFull() {

		complete = true;

		AuthenticationEngine ae = AuthenticationEngine.getInstance();

		ae.removeAllSecurityAssociations();
		mList.clear();
		mListAdapter.notifyDataSetChanged();

		resetTimerAndProgressBar();
	}

	private void resetTimerAndProgressBar() {
		timer.cancel();
		pb.setProgress(0);
		pb.getProgressDrawable().setColorFilter(null);
	}

	private OnTouchListener buttonTouchListener = new OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				complete = false;
				hint.cancel();
				timer = new Timer();
				timer.scheduleAtFixedRate(new myTimerTask() , 0, 10);
				break;
			case MotionEvent.ACTION_UP:
				if (!complete)
					hint.show();
				resetTimerAndProgressBar();
				break;
			default:
				return false;
			}
			return true;
		}
	};

	TrustRelationshipItem lastSelected = null;
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		lastSelected = mList.get(((AdapterContextMenuInfo) menuInfo ).position );
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.trust_relationship_context_menu, menu);
	};

	private void showDetails() {
		SecurityContext ctx = AuthenticationEngine.getInstance().getContextDatabase().getContextByReference(lastSelected.getName());

		Intent i = new Intent(getApplicationContext(), TrustRelationshipDetailActivity.class);
		i.putExtra("context", ctx);

		startActivity(i);

	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.trust_relationship_menu_show_details:
			showDetails();
			return true;
		case R.id.trust_relationship_menu_delete:
			AuthenticationEngine.getInstance().getContextDatabase().removeContext(lastSelected.getName());
			AuthenticationEngine.getInstance().saveSecurityAssociations();
			mList.remove(lastSelected);
			mListAdapter.notifyDataSetChanged();
			return true;
		}
		return super.onContextItemSelected(item);
	}

	private OnItemClickListener itemClick = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			lastSelected = mList.get(position);
			showDetails();
		}
	};
}
