package org.ws4d.mobile.authenticator.ui.trustrelationships;

public class TrustRelationshipItem {

	private String mName = null;
	private String mDetail = null;

	public TrustRelationshipItem() {
		this("", "");
	}

	public TrustRelationshipItem(String name, String detail) {
		mName = name;
		mDetail = detail;
	}

	public String getName() {
		return mName;
	}
	public void setmName(String name) {
		this.mName = name;
	}
	public String getDetail() {
		return mDetail;
	}
	public void setmDetail(String detail) {
		this.mDetail = detail;
	}

}
