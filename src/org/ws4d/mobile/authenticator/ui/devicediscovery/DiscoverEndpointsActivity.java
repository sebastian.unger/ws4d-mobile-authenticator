package org.ws4d.mobile.authenticator.ui.devicediscovery;

import java.util.ArrayList;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.incubation.CredentialManagement.SecurityContext;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.authenticationclient.DefaultAuthenticationClient;
import org.ws4d.java.incubation.wscompactsecurity.authentication.authenticationclient.DiscoveryClient;
import org.ws4d.java.incubation.wscompactsecurity.authentication.authenticationclient.DiscoveryClientCallbacks;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.ClientAuthenticationCallbacks;
import org.ws4d.java.incubation.wspolicy.wspolicyclient.WSSecurityPolicyClient;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.URI;
import org.ws4d.mobile.authenticator.R;
import org.ws4d.mobile.authenticator.ui.flicker.FlickerAuthenticationPluginActivity;
import org.ws4d.mobile.authenticator.ui.pin.PinAuthenticationPluginActivity;
import org.ws4d.mobile.authenticator.ui.tap.TapAuthenticationPluginActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class DiscoverEndpointsActivity extends Activity {

	ArrayList<DeviceItem> mDeviceList = null;
	DeviceListAdapter mDeviceListAdapter = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setTheme(R.style.AppTheme);

		setContentView(R.layout.activity_discover_endpoints);

		mDeviceList = new ArrayList<DeviceItem>();
		mDeviceListAdapter = new DeviceListAdapter(getApplicationContext(), R.layout.device_item_view, mDeviceList);
		ListView lv = (ListView) findViewById(R.id.deviceListView);
		lv.setAdapter(mDeviceListAdapter);
		lv.setOnItemClickListener(itemSelected);
		registerForContextMenu(lv);

		Button b = (Button) findViewById(R.id.discovery_button);
		b.setOnClickListener(this.buttonClickListener);

		ProgressBar pb = (ProgressBar) findViewById(R.id.progressBar1);
		pb.setVisibility(View.INVISIBLE);

		startDiscovery();
	}

	/* Thread to be run to discover devices */
	Runnable discoveryThread = new Runnable() {
		@Override
		public void run() {
			if (!JMEDSFramework.isRunning()) {
				JMEDSFramework.start(null);
			}
			DiscoveryClient dc = new DiscoveryClient(new QName(WSSecurityForDevicesConstants.AuthenticationEndpointType, WSSecurityForDevicesConstants.NAMESPACE));
			dc.setCallbacks(callbacks);
			dc.start(false);
		}
	};

	/* Code-Portion to be run after discovery */
	Runnable postDiscoveryUI = new Runnable() {
		@Override
		public void run() {
			Button b = (Button) findViewById(R.id.discovery_button);
			b.setEnabled(true);
			ProgressBar pb = (ProgressBar) findViewById(R.id.progressBar1);
			pb.setVisibility(View.INVISIBLE);
		}
	};

	/* Code Portion to update list */
	Runnable updateListThread = new Runnable() {
		@Override
		public void run() {
			mDeviceListAdapter.notifyDataSetChanged();
		}
	};

	private void startDiscovery() {
		Button b = (Button) findViewById(R.id.discovery_button);
		b.setEnabled(false);
		ProgressBar pb = (ProgressBar) findViewById(R.id.progressBar1);
		pb.setVisibility(View.VISIBLE);

		mDeviceList.clear();
		runOnUiThread(updateListThread);

		Thread t = new Thread(discoveryThread);
		t.start();
	}

	private ClientAuthenticationCallbacks authenticationcb = new ClientAuthenticationCallbacks() {

		@Override
		public int getOOBpinAsInt(QName mechanism) {
			while (fresh == false) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
				}
			}
			fresh = false;
			return pin;
		}

		@Override
		public void deliverAuthenticationData(SecurityContext ct) {
			AuthenticationEngine.getInstance().getContextDatabase().addContext(ct);
			AuthenticationEngine.getInstance().saveSecurityAssociations();
			runOnUiThread(postAuthentication);
		}

		@Override
		public void prereportMechanism(QName mechanism) {
			deliveredMechanism = mechanism;
			runOnUiThread(startPlugin);
		}

		@Override
		public HashMap receiveAdditionalAuthenticationData() {
			return null;
		}

		@Override
		public void provideAdditionalAuthenticationData(Object key, Object data) {
		}
	};

	Runnable postAuthentication = new Runnable() {

		@Override
		public void run() {
			Toast.makeText(getApplicationContext(), "Successfully authenticated!", Toast.LENGTH_LONG).show();
			startDiscovery();
		}
	};

	QName deliveredMechanism = null;

	Runnable startPlugin = new Runnable() {
		@Override
		public void run() {
			Intent i = null;
			if (deliveredMechanism == null) {
				Toast.makeText(getApplicationContext(), "Error! No mechanism delivered", Toast.LENGTH_LONG).show();
				return;
			} else if (deliveredMechanism.toString().equals(WSSecurityForDevicesConstants.FlickerAuthenticationAsQNameString)) {
				i = new Intent(getApplicationContext(), FlickerAuthenticationPluginActivity.class);
			} else if (deliveredMechanism.toString().equals(WSSecurityForDevicesConstants.PinAuthenticationAsQNameString)) {
				i = new Intent(getApplicationContext(), PinAuthenticationPluginActivity.class);
			} else if (deliveredMechanism.toString().equals(WSSecurityForDevicesConstants.TappingAuthenticationAsQNameString)) {
				i = new Intent(getApplicationContext(), TapAuthenticationPluginActivity.class);
			} else {
				Toast.makeText(getApplicationContext(), "Error! Delivered mechanism cannot be handled", Toast.LENGTH_LONG).show();
				return;
			}
			i.putExtra("org.ws4d.returnresult", true);
			i.putExtra("primary", true);
			i.putExtra("final", true);
			startActivityForResult(i, 2048);
		}
	};

	private int pin = 0;
	private boolean fresh = false;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 2048) {
			if (resultCode == RESULT_OK) {
				this.pin = data.getIntExtra("org.ws4d.intpin", 0);
				Log.d("", "Received Pin: " + this.pin);
				fresh = true;
				synchronized(this) {
					notifyAll();
				}
			}
		}
	};

	int err;

	private Runnable authenticationThread = new Runnable() {
		@Override
		public void run() {
			if (lastSelected == null)
				return;
			DefaultAuthenticationClient client = new DefaultAuthenticationClient();
			client.setAuthenticationCallbacks(authenticationcb);

			/* negotiate algorithms beforehand */
			try {
				client.setSecurityAlgorithmSet(WSSecurityPolicyClient.pickSecurityAlgorithms(lastSelected.getAddresses().toString(),
						AuthenticationEngine.getInstance().getSupportedSignatureAlgorithmsAsString(),
						AuthenticationEngine.getInstance().getSupportedEncryptionAlgorithmsAsString(),
						AuthenticationEngine.getInstance().getSupportedDerivationAlgorithmsAsString()));
			} catch (CommunicationException e) {
				e.printStackTrace();
				return;
				//TODO: Do something dscriptive here...
			}

			err = client.authenticate(client.getDeviceReference(new EndpointReference(new URI(lastSelected.getAddresses())), DPWSCommunicationManager.COMMUNICATION_MANAGER_ID), null, null, true, false);

			if (err != DefaultAuthenticationClient.ERR_NO_ERROR) {
				runOnUiThread(error);

			}

			lastSelected = null;
		}
	};

	private Runnable error = new Runnable() {
		@Override
		public void run() {
			Toast.makeText(getApplicationContext(), "Error while authenticating! Code: " + err, Toast.LENGTH_LONG).show();
		}
	};

	private DeviceItem lastSelected = null;

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		lastSelected = mDeviceList.get(((AdapterContextMenuInfo)menuInfo).position);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.discovered_device_context_menu, menu);
	}

	private void authenticate() {
		Toast.makeText(getApplicationContext(), "Authenticating with " + lastSelected.getAddresses().toString(), Toast.LENGTH_SHORT).show();
		Thread t = new Thread(authenticationThread);
		t.start();
	}

	private void showDetails() {
		LayoutInflater inflater = getLayoutInflater();
		View layout = inflater.inflate(R.layout.device_detail_toast, (ViewGroup) findViewById(R.id.DetailToastRoot));

		TextView tuuid = (TextView) layout.findViewById(R.id.detail_uuid_value);
		tuuid.setText(lastSelected.getAddresses());

		TextView ttype = (TextView) layout.findViewById(R.id.detail_types_value);
		ttype.setText(lastSelected.getTypes());

		Toast toast = new Toast(getApplicationContext());
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.setView(layout);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.show();
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.discovered_device_show_details:
			showDetails();
			break;
		case R.id.discovered_device_authenticate:
			authenticate();
			break;
		default:
			return super.onContextItemSelected(item);
		}
		return true;
	}

	private OnItemClickListener itemSelected = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,	long id) {
			lastSelected = mDeviceList.get(position);
			view.showContextMenu();
		}
	};

	private OnClickListener buttonClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			startDiscovery();
		}
	};

	private String format(QNameSet types) {
		StringBuilder sb = new StringBuilder();
		Iterator it = types.iterator();
		int i = 0;
		while (it.hasNext()) {
			if (i != 0)
				sb.append(",");
			i++;
			sb.append(((QName)it.next()).getLocalPart());
		}
		return sb.toString();
	}

	private DiscoveryClientCallbacks callbacks = new DiscoveryClientCallbacks() {
		@Override
		public void searchTimeOut() {
			runOnUiThread(postDiscoveryUI);
		}
		@Override
		public void deviceFound(DeviceReference devRef) {
			if (devRef.getEndpointReference().getAddress().toString().equals(AuthenticationEngine.getDefaultOwnerID()))
				return;
			DeviceItem d = new DeviceItem();
			QNameSet types = null;
			try {
				types = DiscoveryClient.filterDefaultDeviceTypes(DiscoveryClient.getDeviceTypes(devRef));
			} catch (CommunicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			d.setTypes((types == null) ? "--err--" : format(types));
			d.setAddresses(devRef.getEndpointReference().getAddress().toString());
			d.setIsTrusted(AuthenticationEngine.getInstance().getContextDatabase().getContextById(d.getAddresses())!= null);
			synchronized (mDeviceListAdapter) {
				mDeviceList.add(d);
				runOnUiThread(updateListThread);
			}
		}
	};
}
