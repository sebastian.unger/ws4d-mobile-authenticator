package org.ws4d.mobile.authenticator.ui.devicediscovery;

import java.util.ArrayList;

import org.ws4d.mobile.authenticator.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class DeviceListAdapter extends ArrayAdapter<DeviceItem> {

	ArrayList<DeviceItem> mDeviceList = null;
	Context mContext = null;

	public DeviceListAdapter(Context context, int resource, ArrayList<DeviceItem> list) {
		super(context, resource, list);
		this.mContext = context;
		this.mDeviceList = list;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;

		if (v == null) {
			LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.device_item_view, null);
		}

		final DeviceItem d = mDeviceList.get(position);

		if (d != null) {
			TextView tv = (TextView) v.findViewById(R.id.typeText);
			TextView av = (TextView) v.findViewById(R.id.addressText);
			ImageView iv = (ImageView) v.findViewById(R.id.imageView1);

			if (d.getTypes() != null)
				tv.setText(d.getTypes());

			if (d.getAddresses() != null)
				av.setText(d.getAddresses());

			if (d.isTrusted()) {
				iv.setImageResource(R.drawable.icon_unknown_device_secure);
			} else {
				iv.setImageResource(R.drawable.icon_unknown_device);
			}
		}

		return v;
	}

}
