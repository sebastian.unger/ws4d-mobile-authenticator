package org.ws4d.mobile.authenticator.ui.devicediscovery;

public class DeviceItem {
	private String mTypes;
	private String mAddresses;
	private boolean mTrusted;

	public String getTypes() {
		return mTypes;
	}
	public void setTypes(String types) {
		this.mTypes = types;
	}
	public String getAddresses() {
		return mAddresses;
	}
	public void setAddresses(String addresses) {
		this.mAddresses = addresses;
	}
	public void setIsTrusted(boolean isTrusted) {
		this.mTrusted = isTrusted;
	}
	public boolean isTrusted() {
		return mTrusted;
	}

	public DeviceItem() {
		this(null, null, false);
	}
	public DeviceItem(String types, String addresses) {
		this(types, addresses, false);
	}
	public DeviceItem(String types, String addresses, boolean isTrusted) {
		mTypes = types;
		mAddresses = addresses;
		mTrusted = isTrusted;
	}
}
