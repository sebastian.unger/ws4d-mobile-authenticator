package org.ws4d.mobile.authenticator.ui.flicker;

import java.util.ArrayList;

import org.ws4d.mobile.authenticator.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;

public class BitView extends View {

	private ArrayList<Boolean> mKeySymbols = new ArrayList<Boolean>();
	private ArrayList<Boolean> mLatestKey = new ArrayList<Boolean>();
	private int mState;

	private int arr2int(ArrayList<Boolean> arr) {
		int result = 0;
		int size = arr.size();
		for (int i = 0; i < size; i++) {
			if (arr.get(i))
				result += (int) Math.pow(2, 20 - (i+1));
		}
		return result;
	}

	public void addSymbol(Boolean symbol) {
		mKeySymbols.add(symbol);
		invalidate();
	}

	public void setState(int state) {
		this.mState = state;
		invalidate();
	}

	public void fullKeyRead(Boolean[] key) {
		while (mLatestKey.size() > 0)
			this.mLatestKey.remove(0);
		mLatestKey.addAll(mKeySymbols);
	}

	public void reset() {
		this.mState = BitStreamDetector.STATE_IDLE;
		while (mKeySymbols.size() > 0)
			this.mKeySymbols.remove(0);
		invalidate();
	}

	private Rect mViewRect;

	private Paint mIdlePaint;
	private Paint mLearningPaint;
	private Paint mTransmittingPaint;
	private Paint mHighPaint;
	private Paint mLowPaint;

	private TextPaint mTextPaint;

	private TextPaint mCurPinPaint;

	private TextPaint mLatestPinPaint;

	public BitView(Context context) {
		this(context, null, 0);
	}

	public BitView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public BitView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mViewRect = null;

		mIdlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mIdlePaint.setStyle(Style.FILL);
		mIdlePaint.setColor(0xFFFFC155);

		mLearningPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mLearningPaint.setStyle(Style.FILL);
		mLearningPaint.setColor(0xFF7FD7FF);

		mTransmittingPaint= new Paint(Paint.ANTI_ALIAS_FLAG);
		mTransmittingPaint.setStyle(Style.FILL);
		mTransmittingPaint.setColor(0xFF8B07FF);

		mHighPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mHighPaint.setStyle(Style.FILL);
		mHighPaint.setColor(0xFF00E00E);

		mLowPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mLowPaint.setStyle(Style.FILL);
		mLowPaint.setColor(0xFFCA4141);

		mTextPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
		mTextPaint.setColor(0xFFFFFFFF);

		mCurPinPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
		mCurPinPaint.setColor(0xFF000000);

		mLatestPinPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
		mLatestPinPaint.setColor(0xFF000000);

		this.mState = BitStreamDetector.STATE_IDLE;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		if (mViewRect == null) {
			mViewRect = new Rect();
			getDrawingRect(mViewRect);
		}

		/* Draw State Box */

		Paint upper = null;
		String label = "";
		switch (this.mState) {
		case BitStreamDetector.STATE_IDLE:
			upper = mIdlePaint;
			label = getResources().getString(R.string.idle_label) + "...";
			break;
		case BitStreamDetector.STATE_LEARNING:
			upper = mLearningPaint;
			label = getResources().getString(R.string.learning_label) + "...";
			break;
		case BitStreamDetector.STATE_TRANSMISSION:
			upper = mTransmittingPaint;
			label = getResources().getString(R.string.transmission_label) + "...";
			break;
		default:
			upper = mIdlePaint;
			label = getResources().getString(R.string.idle_label) + "...";
		}

		canvas.drawRect(0, 0, mViewRect.right, mViewRect.bottom / 3 - 1, upper);

		mTextPaint.setTextSize(mViewRect.bottom / 3 - 2);
		float width = mTextPaint.measureText(label);
		canvas.drawText(label, (mViewRect.right - width) / 2, mViewRect.bottom / 3 - 3, mTextPaint);


		/* Draw current bits */
		int i=0;
		int size = mKeySymbols.size();
		for (Boolean symbol : mKeySymbols) {
			canvas.drawRect(i*mViewRect.right / size, mViewRect.bottom / 3, (i+1) * mViewRect.right / size - 1, mViewRect.bottom * 2 / 3 - 1, symbol ? mHighPaint : mLowPaint);
			i++;
		}

		/* Draw current key */
		String curPin = String.valueOf(arr2int(mKeySymbols));
		mCurPinPaint.setTextSize(mViewRect.bottom / 3 - 2);
		width = mCurPinPaint.measureText(curPin);
		canvas.drawText(curPin, (mViewRect.right - width) / 2, (mViewRect.bottom * 2 / 3) - 3, mCurPinPaint);

		/* Draw Latest Key */
		i=0;
		size = mLatestKey.size();
		for (Boolean symbol : mLatestKey) {
			canvas.drawRect(i*mViewRect.right / size, mViewRect.bottom * 2 / 3 , (i+1)*mViewRect.right / size-1, mViewRect.bottom - 1, symbol ? mHighPaint : mLowPaint);
			i++;
		}
		String latestPin = String.valueOf(arr2int(mLatestKey));
		mLatestPinPaint.setTextSize(mViewRect.bottom / 3 - 2);
		width = mLatestPinPaint.measureText(latestPin);
		canvas.drawText(latestPin, (mViewRect.right - width) / 2, mViewRect.bottom - 3, mLatestPinPaint);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		setMeasuredDimension(getMeasuredWidth(), 60);
	}

}
