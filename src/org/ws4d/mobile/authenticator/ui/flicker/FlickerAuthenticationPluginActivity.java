package org.ws4d.mobile.authenticator.ui.flicker;

import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.mobile.authenticator.R;
import org.ws4d.mobile.authenticator.ui.PluginActivity;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

public class FlickerAuthenticationPluginActivity extends PluginActivity {

	private Camera mCamera = null;
	private CameraViewer mCameraView;
	private BitStreamDetector mBitStreamDetector = new BitStreamDetector(this);
	private BitView mBitView;

	Boolean mLastSymbol;
	int mLastState;
	Boolean[] mLastKey;

	/* Operations to be performed on UI after BitStreamDetector read symbols */
	protected Runnable addSymbol = new Runnable() {

		@Override
		public void run() {
			mBitView.addSymbol(mLastSymbol);
		}
	};
	protected Runnable changeState = new Runnable() {

		@Override
		public void run() {
			mBitView.setState(mLastState);
		}
	};
	protected Runnable keyRead = new Runnable() {
		@Override
		public void run() {
			mBitView.fullKeyRead(mLastKey);
			Button b = (Button) findViewById(R.id.reportButton);
			b.setEnabled(true);
			if (getActiveAuthentication() != null) {
				if (getActiveAuthentication().getOOBSharedSecret() == -1) { /* do only for the first key reported */
					int result = 0;
					for (int i = 0; i < mLastKey.length; i++) {
						result += ((mLastKey[i]) ? 1 : 0) * (1 << (mLastKey.length-1-i));
					}
					getActiveAuthentication().setOOBSharedSecret(result);
				}
			}
		}
	};
	protected Runnable reset = new Runnable() {

		@Override
		public void run() {
			mBitView.reset();
		}
	};

	/* Callbacks for keys / symbols read by BitStreamDetector */
	BitStreamDetectorKeyReadCallback mBitStreamDetectorKeyReadCallback = new BitStreamDetectorKeyReadCallback() {
		@Override
		public void onSymbolRead(Boolean symbol) {
			mLastSymbol = symbol;
			runOnUiThread(addSymbol);
		}

		@Override
		public void onStateChanged(int state) {
			mLastState = state;
			runOnUiThread(changeState);
		}

		@Override
		public void onKeyRead(Boolean[] key) {
			mLastKey = key;
			runOnUiThread(keyRead);
		}

		@Override
		public void onReset() {
			runOnUiThread(reset);
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_flicker_authentication_plugin);

		if (getActiveAuthentication() != null) {
			Button b = (Button) findViewById(R.id.launchButton);
			b.setEnabled(true);
		}

		mBitView = (BitView) findViewById(R.id.bitView1);

		setPostReqThread(new Runnable() {
			@Override
			public void run() {
				if (getActiveAuthentication().getOriginMechanism().equals(WSSecurityForDevicesConstants.EncryptedPinExchangeAsQNameString)) {
					reportPinAndFinish(getActiveAuthentication().getOOBSharedSecret());
				} else {
					Toast.makeText(getApplicationContext(), "Done. You may press the report button now.", Toast.LENGTH_SHORT).show();
				}
			}
		});

		if (!checkForCameraHardware(this)) {
			Toast.makeText(this, getResources().getString(R.string.err_no_camera), Toast.LENGTH_LONG).show();
			Button b = (Button) findViewById(R.id.launchButton);
			b.setEnabled(false);
			b = (Button) findViewById(R.id.reportButton);
			b.setEnabled(false);
			return;
		}

		mCamera = getCameraInstance();

		if (mCamera == null) {
			Button b = (Button) findViewById(R.id.launchButton);
			b.setEnabled(false);
			b = (Button) findViewById(R.id.reportButton);
			b.setEnabled(false);
			return;
		}

		mCameraView = new CameraViewer(this, mCamera);

		/* CameraViewer reports EVERY SINGLE PREVIEW FRAME to the BiutStreamCollectorCallback supplied below */
		mCameraView.setBitStreamDetectorCallback(mBitStreamDetector);

		mBitStreamDetector.setBitStreamDetectorKeyReadCallback(mBitStreamDetectorKeyReadCallback);

		FrameLayout preview = (FrameLayout) findViewById(R.id.cameraPreview);

		Toast.makeText(getApplicationContext(), "Aim crosshair so that outer border exactly frames the whole light source.", Toast.LENGTH_SHORT).show();

		preview.addView(mCameraView);
		/* overlay cross hair */
		preview.addView(new CrossHairView(this));

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (mCamera != null) {
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}
	}

	private Camera getCameraInstance() {
		Camera c = null;
		try {
			c = Camera.open(); // attempt to get a Camera instance
		}
		catch (Exception e){
			Toast.makeText(this, getResources().getString(R.string.err_no_camera_availlable), Toast.LENGTH_LONG).show();
		}
		return c; // returns null if camera is unavailable
	}

	private boolean checkForCameraHardware(Context ctx) {
		return ctx.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_flicker_authentication_plugin, menu);
		return true;
	}

	public void onLaunchButtonPressed(View v) {
		Button b = (Button) findViewById(R.id.launchButton);
		b.setEnabled(false);

		startAuthentication();

		Toast.makeText(getApplicationContext(), "Client request started. Flickering will begin shortly.", Toast.LENGTH_LONG).show();

	}

	public void onReportButtonPressed(View v) {
		int result = 0;
		for (int i = 0; i < mLastKey.length; i++) {
			result += ((mLastKey[i]) ? 1 : 0) * (1 << (mLastKey.length-1-i));
		}
		reportPinAndFinish(result);
	}

}
