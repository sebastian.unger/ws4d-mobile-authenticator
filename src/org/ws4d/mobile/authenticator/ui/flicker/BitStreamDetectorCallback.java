package org.ws4d.mobile.authenticator.ui.flicker;

public interface BitStreamDetectorCallback {
	public void onLuminosityMeasured(short relativeLuminosity);
}
