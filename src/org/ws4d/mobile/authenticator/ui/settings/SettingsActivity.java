package org.ws4d.mobile.authenticator.ui.settings;

import java.io.IOException;
import java.util.Set;

import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.securitytokenservice.DefaultSecurityTokenService;
import org.ws4d.java.incubation.wspolicy.policymanagement.PolicyManager;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.util.Log;
import org.ws4d.mobile.authenticator.R;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.MultiSelectListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceGroup;
import android.preference.PreferenceManager;

@SuppressWarnings("deprecation")
public class SettingsActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener {

	public static boolean setJMEDSLogLevel(String level) {
		int logLevel = 4;
		try {
			logLevel = Integer.parseInt(level);
		} catch (NumberFormatException e) {
			Log.warn("Could not parse " + level + " to thorough log level!");
			logLevel = 4;
		}
		switch (logLevel) {
		case 4:
			Log.setLogLevel(Log.DEBUG_LEVEL_DEBUG);
			break;
		case 3:
			Log.setLogLevel(Log.DEBUG_LEVEL_INFO);
			break;
		case 2:
			Log.setLogLevel(Log.DEBUG_LEVEL_WARN);
			break;
		case 1:
			Log.setLogLevel(Log.DEBUG_LEVEL_ERROR);
			break;
		case 0:
			Log.setLogLevel(Log.DEBUG_LEVEL_NO_LOGGING);
			break;
		default:
			Log.warn("Error: Invalid Log Level: " + logLevel);
			return false;
		}
		return true;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		PreferenceManager.setDefaultValues(this, R.xml.preferences,
				false);
		initSummary(getPreferenceScreen());
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		if (key.equals("settings_general_log_level")) {
			setJMEDSLogLevel(sharedPreferences.getString(key, "4"));
		} else if (key.equals("settings_menu_general_mechanisms")) {
			restartAuthenticationService(sharedPreferences.getStringSet(key, null));
		} else if (key.equals("settings_menu_general_address")) {
			AuthenticationEngine.setDefaultOwnerID(sharedPreferences.getString(key, ""));
		}
		updatePrefSummary(findPreference(key));
	}

	private void restartAuthenticationService(Set<String> stringSet) {
		QNameSet mechanisms = new QNameSet();

		if (stringSet == null || stringSet.isEmpty()) {
			Log.warn("Restarting Authentication Service with empty mechanisms set");
		} else {
			for (String s : stringSet) {
				mechanisms.add(new QName(s, WSSecurityForDevicesConstants.NAMESPACE));
				Log.info("Adding mechanism to QNameSet: " + s);
			}
		}

		PolicyManager.initSecurityPolicyManager();
		PolicyManager.getSecurityPolicyManagerInstance().updateLocalAuthenticationMechansims(mechanisms);
		AuthenticationEngine.getInstance().setSupportedOOBauthenticationMechanisms(mechanisms);


		synchronized (DefaultSecurityTokenService.getDefaultSecurityTokenServiceInstance()) {
			try {
				DefaultSecurityTokenService.getDefaultSecurityTokenServiceInstance().stop();
				DefaultSecurityTokenService.getDefaultSecurityTokenServiceInstance().start();
			} catch (IOException e) {
				Log.error("IO Exception while restarting Authentication Service");
			}
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		// Set up a listener whenever a key changes
		getPreferenceScreen().getSharedPreferences()
		.registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		// Unregister the listener whenever a key changes
		getPreferenceScreen().getSharedPreferences()
		.unregisterOnSharedPreferenceChangeListener(this);
	}

	private void initSummary(Preference p) {
		if (p instanceof PreferenceGroup) {
			PreferenceGroup pGrp = (PreferenceGroup) p;
			for (int i = 0; i < pGrp.getPreferenceCount(); i++) {
				initSummary(pGrp.getPreference(i));
			}
		} else {
			updatePrefSummary(p);
		}
	}

	private void updatePrefSummary(Preference p) {
		if (p instanceof ListPreference) {
			ListPreference listPref = (ListPreference) p;
			p.setSummary(listPref.getEntry());
		}
		if (p instanceof EditTextPreference) {
			EditTextPreference editTextPref = (EditTextPreference) p;
			if (p.getTitle().toString().contains("assword"))
			{
				p.setSummary("******");
			} else {
				p.setSummary(editTextPref.getText());
			}
		}
		if (p instanceof MultiSelectListPreference) {
			MultiSelectListPreference pref = (MultiSelectListPreference) p;
			Set<String> set = pref.getValues();

			StringBuilder sb = new StringBuilder();
			boolean bla = false;
			for (String s : set) {
				if (bla) {
					sb.append("|");
				} else {
					bla = true;
				}
				sb.append(s);
			}
			p.setSummary(sb.toString());
		}
	}
}
