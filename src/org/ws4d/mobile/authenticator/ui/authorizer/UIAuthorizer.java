package org.ws4d.mobile.authenticator.ui.authorizer;

import org.ws4d.java.incubation.wscompactsecurity.authorization.engine.AuthorizationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authorization.types.AuthorizationDecision;
import org.ws4d.java.incubation.wscompactsecurity.authorization.types.PasswordUserCredential;
import org.ws4d.mobile.authenticator.R;
import org.ws4d.mobile.authenticator.device.DPWSDeviceService;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class UIAuthorizer extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_uiauthorizer);

		TextView tv = (TextView) findViewById(R.id.ui_authorizer_password_hint_text_view);
		StringBuilder sb = new StringBuilder();
		sb.append("Password hint: ");
		String passwd = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("settings_menu_authorization_Password", "<none set>");
		AuthorizationEngine.getInstance().addUserCredential("password", new PasswordUserCredential(passwd));
		sb.append(passwd);
		tv.setText(sb.toString());

		Bundle bundle = getIntent().getExtras();

		if (bundle != null) {
			String appliesTo = bundle.getString("appliesTo", "<<appliesTo>>");
			String claimDisplayName = bundle.getString("claimDisplayName", "<<claimDisplayName>>");
			String claimDescription = bundle.getString("claimDescription", "<<claimDescription>>");
			String claimValue = bundle.getString("claimValue", "<<claimValue>>");

			StringBuilder sc = new StringBuilder();
			sc.append("An entity wants to access ");
			sc.append(appliesTo);
			sc.append(". It claims the following:\n");
			sc.append(claimDisplayName);
			sc.append("(");
			sc.append(claimDescription);
			sc.append("): ");
			sc.append(claimValue);
			sc.append("Do you want to grant access?");

			TextView ctv = (TextView) findViewById(R.id.ui_authorizer_claims_text_view);

			ctv.setText(sc.toString());
		}

	}

	Messenger mReportBackService = null;
	boolean mServiceBound = false;
	/* Binding to the service is necessary for reporting request results back to device logic */
	private ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mReportBackService = null;
			mServiceBound = false;
			Log.d("WS4D", "FlickerAuthenticationActivity: Unbound from Service");
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mReportBackService = new Messenger(service);
			mServiceBound = true;
			Log.d("WS4D", "FlickerAuthenticationActivity: Bound to Service");
		}
	};
	@Override
	protected void onStart() {
		super.onStart();
		bindService(new Intent(this, DPWSDeviceService.class), mConnection, Context.BIND_AUTO_CREATE);
	}
	@Override
	protected void onStop() {
		super.onStop();
		if (mServiceBound) {
			unbindService(mConnection);
			mServiceBound = false;
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.uiauthorizer, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onPermitButtonPressed(View v) {
		sendAndFinish(AuthorizationDecision.PERMIT);
	}
	public void onDenyButtonPressed(View v) {
		sendAndFinish(AuthorizationDecision.DENY);
	}
	public void onIndeterminateButtonPressed(View v) {
		sendAndFinish(AuthorizationDecision.INDETERMINATE);
	}

	void sendAndFinish(AuthorizationDecision dec) {
		Message msg = Message.obtain(null, DPWSDeviceService.MSG_AUTHORIZATION_DECISION);
		Bundle b = new Bundle();
		b.putSerializable("org.ws4d.authorization", dec);
		msg.setData(b);
		try {
			mReportBackService.send(msg);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finish();
	}

}
