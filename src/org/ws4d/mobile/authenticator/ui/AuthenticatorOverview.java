package org.ws4d.mobile.authenticator.ui;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wspolicy.policymanagement.PolicyManager;
import org.ws4d.mobile.authenticator.R;
import org.ws4d.mobile.authenticator.device.DPWSDeviceService;
import org.ws4d.mobile.authenticator.ui.authorizer.UIAuthorizer;
import org.ws4d.mobile.authenticator.ui.devicediscovery.DiscoverEndpointsActivity;
import org.ws4d.mobile.authenticator.ui.encryptedexchange.EncryptedPINExchangePluginActivity;
import org.ws4d.mobile.authenticator.ui.flicker.FlickerAuthenticationPluginActivity;
import org.ws4d.mobile.authenticator.ui.pin.PinAuthenticationPluginActivity;
import org.ws4d.mobile.authenticator.ui.settings.SettingsActivity;
import org.ws4d.mobile.authenticator.ui.tap.TapAuthenticationPluginActivity;
import org.ws4d.mobile.authenticator.ui.trustrelationships.TrustRelationshipsActivity;
import org.ws4d.wscompactsecurity.authentication.engine.ActiveAuthentication;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class AuthenticatorOverview extends Activity {

	ArrayList<PluginItem> mPluginList = null;
	PluginListAdapter mPluginListAdapter = null;

	ActiveAuthentication mActiveAuthentication = null;

	public boolean isServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (DPWSDeviceService.class.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	private boolean mIsDeviceRunning = false;
	public boolean isDeviceRunning() {
		return mIsDeviceRunning;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_authenticator_overview);

		mPluginList = new ArrayList<PluginItem>();

		mPluginListAdapter = new PluginListAdapter(this, R.layout.plugin_item_view, mPluginList);

		ListView lv = (ListView) findViewById(R.id.pluginListView);
		lv.setAdapter(mPluginListAdapter);

		registerPlugins();

		mPluginListAdapter.notifyDataSetChanged();

		if (!isServiceRunning()) {
			Intent deviceServiceIntent = new Intent(this, DPWSDeviceService.class);
			startService(deviceServiceIntent);
		}

		Bundle bundle = getIntent().getExtras();

		if (bundle != null) {
			mActiveAuthentication = (ActiveAuthentication) bundle.getSerializable("org.ws4d.authentication");
		}

		if (mActiveAuthentication != null ) {
			if (mActiveAuthentication.getOriginMechanism().equals(WSSecurityForDevicesConstants.EncryptedPinExchangeAsQNameString)) {
				Toast.makeText(getApplicationContext(), "PIN got encrypted and bypassed. You may quit.", Toast.LENGTH_LONG).show();
			} else {
				Button b = (Button) findViewById(R.id.overviewProcessButton);
				b.setVisibility(Button.VISIBLE);
				b.setBackgroundResource(R.drawable.button_background);
				b.setTextColor(Color.WHITE);
				b.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						if (mActiveAuthentication.getOriginMechanism().equals(WSSecurityForDevicesConstants.PinAuthenticationAsQNameString)) {

							Intent i = new Intent(getApplicationContext(), PinAuthenticationPluginActivity.class);
							i.putExtras(getIntent());
							i.putExtra("primary", false);
							startActivity(i);
							finish();

						} else if (mActiveAuthentication.getOriginMechanism().equals(WSSecurityForDevicesConstants.TappingAuthenticationAsQNameString)) {

							Intent i = new Intent(getApplicationContext(), TapAuthenticationPluginActivity.class);
							i.putExtras(getIntent());
							i.putExtra("primary", false);
							startActivity(i);
							finish();

							//        			} else if () { Something else

						}

					}
				});
				b.setOnTouchListener(new OnTouchListener() {
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						switch (event.getAction()) {
						case MotionEvent.ACTION_DOWN:
							((Button)v).setBackgroundResource(R.drawable.button_background_pressed);
							break;
						case MotionEvent.ACTION_UP:
							((Button)v).setBackgroundResource(R.drawable.button_background);
							break;
						}
						return false;
					}
				});
			}
		}
		SettingsActivity.setJMEDSLogLevel(PreferenceManager.getDefaultSharedPreferences(this).getString("settings_general_log_level", "4"));
	}

	private void registerPlugins() {
		//TODO: Replace by *real* plugin mechanism (one day in the very distant future)
		mPluginList.add(new PluginItem(
				getResources().getString(R.string.flicker_plugin_name),
				getResources().getString(R.string.flicker_plugin_description),
				getResources().getDrawable(R.drawable.icon_light_bulb),
				"",
				new Intent(this, FlickerAuthenticationPluginActivity.class)
				));
		mPluginList.add(new PluginItem(
				"QR Code Authentication",
				"Authenticate via a display QR Code",
				getResources().getDrawable(R.drawable.icon_qr_code),
				"",
				null
				));
		mPluginList.add(new PluginItem(
				"NFC Authentication",
				"Use near field communication to authenticate",
				getResources().getDrawable(R.drawable.icon_nfc),
				"",
				null
				));
		mPluginList.add(new PluginItem(
				"USB Authentication",
				"Use a USB connection to authenticate",
				getResources().getDrawable(R.drawable.icon_usb),
				"",
				null
				));
		mPluginList.add(new PluginItem(
				"PIN Authentication",
				"Authenticate by a displayed PIN",
				getResources().getDrawable(R.drawable.icon_pin),
				"",
				new Intent(getApplicationContext(), PinAuthenticationPluginActivity.class)
				));
		mPluginList.add(new PluginItem(
				"Tap Authentication",
				"Authenticate via Tapping a Switch",
				getResources().getDrawable(R.drawable.icon_switch),
				"",
				new Intent(getApplicationContext(), TapAuthenticationPluginActivity.class)
				));
		mPluginList.add(new PluginItem(
				"Encrypted Pin Exchange",
				"More transparent for users if Authenticators are authenticated",
				getResources().getDrawable(R.drawable.icon_encx),
				"",
				new Intent(getApplicationContext(), EncryptedPINExchangePluginActivity.class)
				));

		//    	mPluginList.add(new PluginTemplate("name","description", intent)); /* check class file PluginTemplate for documentation */
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_authenticator_overview, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_settings:
			Intent settingsActivity = new Intent(getApplicationContext(), SettingsActivity.class);
			startActivity(settingsActivity);
			return true;
		case R.id.menu_start_stop_service:
			if (!isDeviceRunning()) {
				startDevice();
				item.setTitle(R.string.menu_stop_service);
				Toast.makeText(getApplicationContext(), "(Re)started Device", Toast.LENGTH_SHORT).show();
			} else {
				PolicyManager.getSecurityPolicyManagerInstance().clearRemotes();
				stopDevice();
				item.setTitle(R.string.menu_start_service);
				Toast.makeText(getApplicationContext(), "Stopped Device", Toast.LENGTH_SHORT).show();
			}
			return true;
		case R.id.menu_discover_endpoints:
			Intent i = new Intent(getApplicationContext(), DiscoverEndpointsActivity.class);
			startActivity(i);
			return true;
		case R.id.menu_show_trust_relationships:
			Intent j = new Intent(getApplicationContext(), TrustRelationshipsActivity.class);
			startActivity(j);
			return true;
		case R.id.menu_reset_authentication_engine:
			AuthenticationEngine.getInstance().reset();
			Toast.makeText(getApplicationContext(), "Resetting Authentication Engine...", Toast.LENGTH_SHORT).show();
			return true;
		case R.id.show_ui_authorizer:
			Intent k = new Intent(getApplicationContext(), UIAuthorizer.class);
			startActivity(k);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/*
	 * The following is to connect to the service hosting the DPWS Device, so the DPWS device
	 * can be 'remotely' controlled (e.g. stopped and restarted)
	 */
	Messenger mDPWSDeviceService = null;
	boolean mServiceBound = false;

	/* Binding to the service is necessary for reporting request results back to device logic */
	private ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mDPWSDeviceService = null;
			mServiceBound = false;
			Log.d("WS4D", "AuthenticationOverview: Unbound from Service");
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mDPWSDeviceService = new Messenger(service);
			mServiceBound = true;
			Log.d("WS4D", "AuthenticationOverview: Bound to Service");
			startDevice();
		}
	};

	void startDevice() {
		if (mServiceBound) {
			Message msg = Message.obtain(null, DPWSDeviceService.MSG_START_DEVICE);
			try {
				msg.replyTo = new Messenger(resHandler);
				mDPWSDeviceService.send(msg);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	void stopDevice() {
		if (mServiceBound) {
			Message msg = Message.obtain(null, DPWSDeviceService.MSG_STOP_DEVICE);
			try {
				msg.replyTo = new Messenger(resHandler);
				mDPWSDeviceService.send(msg);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	void refreshDeviceState() {
		if (mServiceBound) {
			Message msg = Message.obtain(null, DPWSDeviceService.MSG_GET_DEVICE_STATE);
			try {
				msg.replyTo = new Messenger(resHandler);
				mDPWSDeviceService.send(msg);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		bindService(new Intent(this, DPWSDeviceService.class), mConnection, Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (mServiceBound) {
			unbindService(mConnection);
			mServiceBound = false;
		}
	}

	ResponseHandler resHandler = new ResponseHandler(this);
	static class ResponseHandler extends Handler {
		private WeakReference<AuthenticatorOverview> mAuthenticatorOverview = null;

		public ResponseHandler(AuthenticatorOverview ao) {
			mAuthenticatorOverview = new WeakReference<AuthenticatorOverview>(ao);
		}

		@Override
		public void handleMessage(Message msg) {
			int respCode = msg.what;
			switch (respCode) {
			case DPWSDeviceService.MSG_DEVICE_STATE_OFFLINE_RESPONSE:
				mAuthenticatorOverview.get().mIsDeviceRunning = false;
				break;
			case DPWSDeviceService.MSG_DEVICE_STATE_ONLINE_RESPONSE:
				mAuthenticatorOverview.get().mIsDeviceRunning = true;
				break;
			default:
				super.handleMessage(msg);
			}
		}
	}
}
