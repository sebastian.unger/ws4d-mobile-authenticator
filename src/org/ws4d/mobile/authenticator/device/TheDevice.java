package org.ws4d.mobile.authenticator.device;

import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.ServiceAuthenticationCallbacks;
import org.ws4d.java.incubation.wscompactsecurity.authorization.types.AsynchronousDecisionCallbacks;
import org.ws4d.java.service.DefaultDevice;
import org.ws4d.java.structures.ReadOnlyIterator;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.wscompactsecurity.authentication.OperationDataCallback;
import org.ws4d.wscompactsecurity.securitytokenservice.SecurityTokenService;

public class TheDevice extends DefaultDevice {

	public TheDevice() {
		this(null, null, null, null);
	}

	public TheDevice(OperationDataCallback cb, ServiceAuthenticationCallbacks sacb, AsynchronousDecisionCallbacks adcb, QNameSet additional) {
		super(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		this.setBasicPortTypes();

		if (additional != null) {
			this.setAdditionalPortTypes(additional);
		}

		this.addFriendlyName("en-US", "Android DPWS Device");

		this.addManufacturer("en-US", "ws4d.org");

		this.addModelName("en-US", "Rev. 01");

		this.addService(new SecurityTokenService(cb, sacb, adcb));
	}

	public TheDevice(OperationDataCallback cb, ServiceAuthenticationCallbacks sacb, AsynchronousDecisionCallbacks adcb) {
		this(cb, sacb, adcb, null);
	}

	public void setBasicPortTypes() {
		QNameSet portTypes = new QNameSet();
		portTypes.add(new QName(org.ws4d.mobile.authenticator.device.Constants.PortType, org.ws4d.mobile.authenticator.device.Constants.Namespace));
		this.setPortTypes(portTypes);
	}

	public void setAdditionalPortTypes(QNameSet addtionalPortTypes) {

		QNameSet completePortTypes = addtionalPortTypes;

		ReadOnlyIterator portTypes = (ReadOnlyIterator) this.getPortTypes();

		while (portTypes.hasNext()) {
			completePortTypes.add((QName) portTypes.next());
		}

		this.setPortTypes(completePortTypes);
	}

	public void resetPortTypes() {
		setBasicPortTypes();
	}

}
