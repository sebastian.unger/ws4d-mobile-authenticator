package org.ws4d.mobile.authenticator.device;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Random;
import java.util.Set;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.android.Util;
import org.ws4d.java.incubation.Types.SecurityAlgorithmSet;
import org.ws4d.java.incubation.WSSecurityForDevices.AlgorithmConstants;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.ServiceAuthenticationCallbacks;
import org.ws4d.java.incubation.wscompactsecurity.authorization.types.AsynchronousDecisionCallbacks;
import org.ws4d.java.incubation.wscompactsecurity.authorization.types.AuthorizationDecision;
import org.ws4d.java.incubation.wspolicy.policymanagement.PolicyManager;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.util.IDGenerator;
import org.ws4d.mobile.authenticator.R;
import org.ws4d.mobile.authenticator.client.Client;
import org.ws4d.mobile.authenticator.ui.authorizer.UIAuthorizer;
import org.ws4d.mobile.authenticator.ui.encryptedexchange.EncryptedPINExchangePluginActivity;
import org.ws4d.mobile.authenticator.ui.flicker.FlickerAuthenticationPluginActivity;
import org.ws4d.mobile.authenticator.ui.pin.PinAuthenticationPluginActivity;
import org.ws4d.mobile.authenticator.ui.test.TestOperationActivity;
import org.ws4d.wscompactsecurity.authentication.CallbackDataObject;
import org.ws4d.wscompactsecurity.authentication.OperationDataCallback;
import org.ws4d.wscompactsecurity.authentication.engine.ActiveAuthentication;
import org.ws4d.wscompactsecurity.authentication.engine.AndroidSimpleContextDBPersistence;
import org.ws4d.wscompactsecurity.authentication.test.TestOperationData;
import org.ws4d.wscompactsecurity.authentication.test.ToggleOperationData;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.BigTextStyle;
import android.support.v4.app.NotificationCompat.Builder;
import android.util.Log;
import android.widget.Toast;

//TODO: Come up w/ sth/ else e.g. use a lovely settings class!!
@SuppressLint("WorldReadableFiles")
public class DPWSDeviceService extends Service {

	public static final int MSG_TEST_CLIENT_REPORT_BACK = 1;
	public static final int MSG_AUTHENTICATION_CLIENT_REPORT_BACK = 2;
	public static final int MSG_START_DEVICE = 3;
	public static final int MSG_STOP_DEVICE = 4;
	public static final int MSG_GET_DEVICE_STATE = 5;
	public static final int MSG_DEVICE_STATE_ONLINE_RESPONSE = 6;
	public static final int MSG_DEVICE_STATE_OFFLINE_RESPONSE = 7;
	public static final int MSG_AUTHORIZATION_DECISION = 8;
	private static int mRequestId = 1;

	private static TheDevice d = null;
	public static TheDevice getDevice() {
		return d;
	}
	boolean mWaitingForResult = false;
	boolean mWaitingForAuthenticationResult = false;
	boolean freshAuthenticationData = false;

	boolean mFreshAuthorization = false;
	AuthorizationDecision mFreshAuthorizationDecision = AuthorizationDecision.INDETERMINATE;

	CallbackDataObject mClientOperationData = null;
	ActiveAuthentication mActiveAuthentication = null;

	private boolean isFakeDeviceActive() {
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		return settings.getBoolean("fakeDeviceActive", false);
	}

	private QNameSet getFakePortTypes() {
		//TODO: Read from Prefs File
		return new QNameSet(new QName("LightbulbDevice", "http://www.ws4d.org"));
	}

	ServiceAuthenticationCallbacks sacb = new ServiceAuthenticationCallbacks() {
		@Override
		public void populatePin(int pin, QName mechanism) {
			/* Build the notification to inform the user */
			/*   compact form: */
			NotificationCompat.Builder mBuilder = new Builder(getApplicationContext());
			mBuilder.setSmallIcon(R.drawable.ic_launcher);
			mBuilder.setContentTitle("Direct Authentication Request!");
			mBuilder.setContentText("Mechanism: " + mechanism.toString() + "*wisper* pin: " + pin);

			/*   detailed form: */
			NotificationCompat.BigTextStyle style = new BigTextStyle();
			style.setBigContentTitle("New Direct Authentication Request");
			style.bigText("A previously unknown device desires to directly authenticate with the entity in "
					+ "your hand. If it pleases you to grant the requestor's wish then you might acknowledge "
					+ "this notification by gently tapping it with the tip of one of your fingers. Be safe!");

			mBuilder.setStyle(style);

			/* assemble intent */

			Intent intent = null;
			ActiveAuthentication data = new ActiveAuthentication();
			data.setOOBSharedSecret(pin);

			if (mechanism.toString().equals(WSSecurityForDevicesConstants.FlickerAuthenticationAsQNameString)) {

				intent = new Intent(getApplicationContext(), FlickerAuthenticationPluginActivity.class);

				intent.putExtra("org.ws4d.authentication", data);

			} else if (mechanism.toString().equals(WSSecurityForDevicesConstants.PinAuthenticationAsQNameString)) {
				intent = new Intent(getApplicationContext(), PinAuthenticationPluginActivity.class);
				intent.putExtra("org.ws4d.authentication",	data);
				intent.putExtra("fake", false);
				intent.putExtra("primary", false);
				intent.putExtra("final", true);
				//			} else if (anotherMechanism) { /* here be more mechanisms */

			} else {
				/* maybe some error message ..? */
			}

			if (intent != null) {
				mBuilder.setContentIntent(PendingIntent.getActivity(getApplicationContext(), mRequestId++, intent, 0));
			}

			mBuilder.setAutoCancel(true);
			mBuilder.setPriority(Notification.PRIORITY_HIGH);
			mBuilder.setDefaults(Notification.DEFAULT_ALL);

			NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			mNotificationManager.notify((int)Math.random(), mBuilder.build());
		}

		@Override
		public void authenticationResult(EndpointReference ep, boolean success) {
			daEP = ep;
			wasItASuccess = success;
			Handler handler = new Handler(Looper.getMainLooper());
			handler.post(postAuthenticationThread);
		}
	};

	EndpointReference daEP = null;
	boolean wasItASuccess = false;

	Runnable postAuthenticationThread = new Runnable() {
		@Override
		public void run() {
			Toast.makeText(getApplicationContext(), "Authentication with "
					+ daEP.getAddress().toString()
					+ ((wasItASuccess) ? " " : "not ")
					+ "successful!", Toast.LENGTH_SHORT).show();
		}
	};

	@SuppressLint("InlinedApi")
	OperationDataCallback cb = new OperationDataCallback() {
		@Override
		public String testOperationSetData(CallbackDataObject data) {
			if (data != null) {
				TestOperationData toData = (TestOperationData) data.getObject();
				Log.d("ws4d", "in: " + toData.getIn() + "; out: " + toData.getOut());

				NotificationCompat.Builder mBuilder = new Builder(getApplicationContext());
				mBuilder.setSmallIcon(R.drawable.ic_launcher);
				mBuilder.setContentTitle("Received Toggle Request!");
				mBuilder.setContentText("in: " + toData.getIn() + "; out: " + toData.getOut());

				Intent intent = new Intent(getApplicationContext(), TestOperationActivity.class);
				intent.putExtra("URI", toData.getIn());

				mBuilder.setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, intent, 0));

				mBuilder.setAutoCancel(true);
				mBuilder.setPriority(Notification.PRIORITY_HIGH);
				mBuilder.setDefaults(Notification.DEFAULT_ALL);

				NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
				mNotificationManager.notify(1, mBuilder.build());

				mWaitingForResult = true;

				while (mClientOperationData == null) {
					/* I really feel bad for this but I desperately need to get this
					 * working. Only thing I'm up to right now is leaving a FIXME!!
					 */
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				String result = ((ToggleOperationData)mClientOperationData).getResult();

				mClientOperationData = null;
				mWaitingForResult = false;

				return result;

			}
			return "";
		}

		@Override
		public ActiveAuthentication authenticationCallback1(
				ActiveAuthentication data) {

			/* Build the notification to inform the user */
			/*   compact form: */
			NotificationCompat.Builder mBuilder = new Builder(getApplicationContext());
			mBuilder.setSmallIcon(R.drawable.ic_launcher);
			mBuilder.setContentTitle("Authentication Request!");
			mBuilder.setContentText("in: " + data.getOrigin() + "; out: " + data.getTarget());

			/*   detailed form: */
			NotificationCompat.BigTextStyle style = new BigTextStyle();
			style.setBigContentTitle("New Authentication Request");
			style.bigText("Origin: " + data.getOrigin() +
					"\n  (unknown type), ui: " + data.getOriginMechanism() +
					"\nTarget: " + data.getTarget() +
					"\n  (" + data.getTargetDeviceType() + "), ui: " + data.getTargetMechanism()
					+ ((data.getIntermediary() == null) ? "" : "VIA: " + data.getIntermediary()));

			mBuilder.setStyle(style);

			/* assemble intent */

			Intent intent = null;

			if (isFakeDeviceActive()) {
				intent = new Intent(getApplicationContext(), PinAuthenticationPluginActivity.class);
				intent.putExtra("org.ws4d.authentication", data);
				intent.putExtra("fake", true);
				intent.putExtra("primary", true);
			} else if (data.getTargetMechanism().equals(WSSecurityForDevicesConstants.FlickerAuthenticationAsQNameString)) {

				intent = new Intent(getApplicationContext(), FlickerAuthenticationPluginActivity.class);
				intent.putExtra("org.ws4d.authentication", data);
				intent.putExtra("primary", true);

			} else if (data.getTargetMechanism().equals(WSSecurityForDevicesConstants.PinAuthenticationAsQNameString)) {
				intent = new Intent(getApplicationContext(), PinAuthenticationPluginActivity.class);
				intent.putExtra("org.ws4d.authentication",	data);
				intent.putExtra("fake", false);
				intent.putExtra("primary", true);
			} else if (data.getTargetMechanism().equals(WSSecurityForDevicesConstants.EncryptedPinExchangeAsQNameString)) {
				intent = new Intent(getApplicationContext(), EncryptedPINExchangePluginActivity.class);
				intent.putExtra("org.ws4d.authentication",	data);
				intent.putExtra("fake", false);
				intent.putExtra("primary", true);

				//			} else if (anotherMechanism) { /* here be more mechanisms */
			} else {
				/* maybe some error message ..? */
			}

			if (intent != null) {
				mBuilder.setContentIntent(PendingIntent.getActivity(getApplicationContext(), mRequestId++, intent, 0));
			}

			mBuilder.setAutoCancel(true);
			mBuilder.setPriority(Notification.PRIORITY_HIGH);
			mBuilder.setDefaults(Notification.DEFAULT_ALL);

			NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			mNotificationManager.notify((int)Math.random(), mBuilder.build());

			mWaitingForAuthenticationResult = true;

			while (!freshAuthenticationData) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			freshAuthenticationData = false;

			return mActiveAuthentication;
		}

		@Override
		public ActiveAuthentication authenticationCallback2(
				ActiveAuthentication data) {
			return Client.authenticateECCDH(data, isFakeDeviceActive());
		}
	};

	AsynchronousDecisionCallbacks adcb = new AsynchronousDecisionCallbacks() {

		@Override
		public AuthorizationDecision getAsynchronousDecision(ParameterValue request) {

			mFreshAuthorization = false;

			String appliesTo = ParameterValueManagement.getString(request,
					Constants.AppliesTo.getLocalPart());

			String claimDisplayName = ParameterValueManagement.getString(request,
					Constants.Claims.getLocalPart() + "/" +
							Constants.Claim.getLocalPart() + "/" +
							Constants.DisplayName.getLocalPart());
			String claimDescription = ParameterValueManagement.getString(request,
					Constants.Claims.getLocalPart() + "/" +
							Constants.Claim.getLocalPart() + "/" +
							Constants.Description.getLocalPart());
			String claimValue = ParameterValueManagement.getString(request,
					Constants.Claims.getLocalPart() + "/" +
							Constants.Claim.getLocalPart() + "/" +
							Constants.Value.getLocalPart());

			Intent requestAuthorizationIntent = new Intent(getApplicationContext(), UIAuthorizer.class);

			requestAuthorizationIntent.putExtra("appliesTo", appliesTo);
			requestAuthorizationIntent.putExtra("claimDisplayName", claimDisplayName);
			requestAuthorizationIntent.putExtra("claimDescription", claimDescription);
			requestAuthorizationIntent.putExtra("claimValue", claimValue);


			/* Build the notification to inform the user */
			/*   compact form: */
			NotificationCompat.Builder mBuilder = new Builder(getApplicationContext());
			mBuilder.setSmallIcon(R.drawable.ic_launcher);
			mBuilder.setContentTitle("Authentication Request!");
			mBuilder.setContentText("Tap to check");

			/*   detailed form: */
			NotificationCompat.BigTextStyle style = new BigTextStyle();
			style.setBigContentTitle("New Authentication Request");
			style.bigText("Origin: " + claimValue +
					"\nTarget: " + appliesTo);

			mBuilder.setStyle(style);

			if (requestAuthorizationIntent != null) {
				mBuilder.setContentIntent(PendingIntent.getActivity(getApplicationContext(), mRequestId++, requestAuthorizationIntent, 0));
			}

			mBuilder.setAutoCancel(true);
			mBuilder.setPriority(Notification.PRIORITY_HIGH);
			mBuilder.setDefaults(Notification.DEFAULT_ALL);

			NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			mNotificationManager.notify((int)Math.random(), mBuilder.build());

			while (!mFreshAuthorization) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			AuthorizationDecision result = mFreshAuthorizationDecision;

			mFreshAuthorization = false;

			return result;
		}
	};

	static class IncomingHandler extends Handler {
		/* This one receives messages from client implementations.
		 * This way, client implementations can report back any results
		 */

		/* about that referencing thing:
		 * 
		 * Class needs to be static because otherwise memory leaks
		 * are possible. By using the weak reference to the DPWSDeviceService
		 * object, it is still possible to refer to its members
		 */

		private WeakReference<DPWSDeviceService> mService = null;

		public IncomingHandler(DPWSDeviceService service) {
			mService = new WeakReference<DPWSDeviceService>(service);
		}

		@Override
		public void handleMessage(Message msg) {
			Bundle b = msg.getData();
			Message response = null;
			switch (msg.what) {
			case MSG_TEST_CLIENT_REPORT_BACK:

				ToggleOperationData data = (ToggleOperationData) b.getSerializable("data");
				if (mService.get().mWaitingForResult) {
					mService.get().mClientOperationData = data;
				}

				Log.d("WS4D", "Got Service Message! " + data.getResult());
				break;
			case MSG_AUTHENTICATION_CLIENT_REPORT_BACK:

				ActiveAuthentication aa = (ActiveAuthentication) b.getSerializable("org.ws4d.authentication");

				if (mService.get().mWaitingForAuthenticationResult) {
					mService.get().mActiveAuthentication = aa;
				}

				mService.get().freshAuthenticationData = true;

				Log.d("WS4D", "Got Authentication Data");
				break;
			case MSG_STOP_DEVICE:
				mService.get().stopDevice();
				response = Message.obtain(null, MSG_DEVICE_STATE_OFFLINE_RESPONSE);
				try {
					msg.replyTo.send(response);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case MSG_START_DEVICE:
				mService.get().startDevice();
				response = Message.obtain(null, MSG_DEVICE_STATE_ONLINE_RESPONSE);
				try {
					msg.replyTo.send(response);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case MSG_GET_DEVICE_STATE:
				if (DPWSDeviceService.d != null && DPWSDeviceService.d.isRunning())
					response = Message.obtain(null, MSG_DEVICE_STATE_ONLINE_RESPONSE);
				else
					response = Message.obtain(null, MSG_DEVICE_STATE_OFFLINE_RESPONSE);
				try {
					msg.replyTo.send(response);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case MSG_AUTHORIZATION_DECISION:
				AuthorizationDecision ad = (AuthorizationDecision) b.getSerializable("org.ws4d.authorization");
				mService.get().mFreshAuthorizationDecision = ad;
				mService.get().mFreshAuthorization = true;
				Log.d("ws4d", "Received decision: " + ad.toString());
				break;
			default:
				super.handleMessage(msg);
				break;
			}

		}
	}
	final Messenger mMessenger = new Messenger(new IncomingHandler(this));

	public DPWSDeviceService() {
		super();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Random r = new Random(System.currentTimeMillis());
		mRequestId = r.nextInt();
	}

	private void startDevice() {
		if (!JMEDSFramework.isRunning()) { /* start framework and Device */
			JMEDSFramework.start(null);
		}

		Set<String> mechanismStrings = PreferenceManager.getDefaultSharedPreferences(this).getStringSet("settings_menu_general_mechanisms", null);
		QNameSet mechanisms = new QNameSet();


		if (mechanismStrings == null || mechanismStrings.isEmpty()) {
			Log.w("DPWSDeviceService", "Starting Authentication Service with empty mechanisms set");
		} else {
			for (String s : mechanismStrings) {
				mechanisms.add(new QName(s, WSSecurityForDevicesConstants.NAMESPACE));
				Log.i("DPWSDeviceService", "Adding mechanism to QNameSet: " + s);
			}
		}

		PolicyManager.initSecurityPolicyManager();
		PolicyManager.getSecurityPolicyManagerInstance().setLocalAuthenticationMechanisms(mechanisms);

		AuthenticationEngine ae = AuthenticationEngine.getInstance();

		ae.setSupportedOOBauthenticationMechanisms(mechanisms);

		//FIXME!! Make sure they get started only once...
		ae.addSupportedAlgorithmsSet(
				new SecurityAlgorithmSet(
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_SIG_SHA1_AES_CBC128),
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_ENC_AES_CBC128)
						)
				);
		ae.addSupportedAlgorithmsSet(
				new SecurityAlgorithmSet(
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_SIG_SHA1_RC4),
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_ENC_RC4)
						)
				);
		ae.addSupportedAlgorithmsSet(
				new SecurityAlgorithmSet(
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_SIG_SHA1_AES_CBC128),
						null
						)
				);
		ae.addSupportedAlgorithmsSet(
				new SecurityAlgorithmSet(
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_SIG_SHA1_RC4),
						null
						)
				);

		if (d == null) {
			d = new TheDevice(cb, sacb, adcb, isFakeDeviceActive() ? getFakePortTypes() : null);
		}

		d.setEndpointReference(getEndpointReference());

		AuthenticationEngine.setDefaultOwnerID(getEndpointReference().getAddress().toString());

		((AndroidSimpleContextDBPersistence)ae.getPersistence()).setContext(getApplicationContext());
		ae.setDbName("contexts.db");

		ae.loadSecurityAssociations();


		if (!d.isRunning()) {
			try {
				d.start();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.d("ws4d", "Service started... I should be discoverable...");
			Toast.makeText(this, "Device started in background...", Toast.LENGTH_SHORT).show();

		}
	}

	private EndpointReference getEndpointReference() {
		String suri = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("settings_menu_general_address", "");
		AttributedURI uuri;
		if (suri == null || suri.isEmpty()) {
			uuri = new AttributedURI(IDGenerator.getUUIDasURI());
			Editor ed = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
			ed.putString("settings_menu_general_address", uuri.toString());
			ed.commit();
		} else {
			uuri = new AttributedURI(suri);
		}
		return new EndpointReference(uuri);
	}

	private void stopDevice() {
		try {
			if (d != null) {
				if (d.isRunning()) {
					d.sendBye();
					d.stop(true);
				}
				d = null;
			}


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Log.d("ws4d", "Service stawped... I should no longer be discoverable...");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Util.initAll(getApplicationContext());
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mMessenger.getBinder();
	}

	@Override
	public void onDestroy() {
		stopDevice();
		super.onDestroy();
	}


}
