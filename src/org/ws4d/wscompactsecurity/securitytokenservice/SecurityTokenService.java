package org.ws4d.wscompactsecurity.securitytokenservice;

import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.incubation.Types.SecurityTokenServiceType;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.ServiceAuthenticationCallbacks;
import org.ws4d.java.incubation.wscompactsecurity.authorization.requestauthorizationoperation.DefaultRequestAuthorizationOperation;
import org.ws4d.java.incubation.wscompactsecurity.authorization.types.AsynchronousDecisionCallbacks;
import org.ws4d.java.incubation.wscompactsecurity.securitytokenservice.DefaultSecurityTokenService;
import org.ws4d.wscompactsecurity.authentication.OperationDataCallback;
import org.ws4d.wscompactsecurity.authentication.eccdh.ECC_DH1;
import org.ws4d.wscompactsecurity.authentication.eccdh.ECC_DH2;
import org.ws4d.wscompactsecurity.authentication.test.TestOperation;

public class SecurityTokenService extends DefaultSecurityTokenService {

	public SecurityTokenService() {
		this(null, null, null);
	}

	public SecurityTokenService(OperationDataCallback cb, ServiceAuthenticationCallbacks sacb) {
		this(cb, sacb, null);
	}

	public SecurityTokenService(OperationDataCallback cb, ServiceAuthenticationCallbacks sacb, AsynchronousDecisionCallbacks adcb) {

		super(SecurityTokenServiceType.UIAUTHENTICATOR | SecurityTokenServiceType.ENDPOINT | SecurityTokenServiceType.ASYNCHRONOUSUIAUTHORIZER, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		this.addOperation(new TestOperation(cb));

		/* ECC-DH */
		this.addOperation(new ECC_DH1(cb, sacb));
		this.addOperation(new ECC_DH2(cb, sacb));

		/* Authorization */
		this.addOperation(new DefaultRequestAuthorizationOperation(adcb));

	}

}
