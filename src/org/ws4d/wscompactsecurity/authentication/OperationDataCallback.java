package org.ws4d.wscompactsecurity.authentication;

import org.ws4d.wscompactsecurity.authentication.engine.ActiveAuthentication;

public interface OperationDataCallback {
	public String testOperationSetData(CallbackDataObject data);
	public ActiveAuthentication authenticationCallback1(ActiveAuthentication data);
	public ActiveAuthentication authenticationCallback2(ActiveAuthentication data);
}
