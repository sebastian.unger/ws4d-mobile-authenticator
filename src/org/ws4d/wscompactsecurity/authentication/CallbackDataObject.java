package org.ws4d.wscompactsecurity.authentication;

import java.io.Serializable;

public interface CallbackDataObject extends Serializable{
	public CallbackDataObject getObject();
}
