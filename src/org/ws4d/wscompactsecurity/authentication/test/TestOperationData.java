package org.ws4d.wscompactsecurity.authentication.test;

import org.ws4d.wscompactsecurity.authentication.CallbackDataObject;

public class TestOperationData implements CallbackDataObject {

	final static long serialVersionUID = 1L;
	
	private String in = "";
	private String out = "";
	
	public TestOperationData() {
		
	}
	public TestOperationData(String in) {
		this.in = in;
	}
	public TestOperationData(String in, String out) {
		this.in = in;
		this.out = out;
	}
	
	@Override
	public CallbackDataObject getObject() {
		// TODO Auto-generated method stub
		return this;
	}

	public String getOut() {
		return out;
	}

	public void setOut(String out) {
		this.out = out;
	}

	public String getIn() {
		return in;
	}

	public void setIn(String in) {
		this.in = in;
	}
	
	

}
