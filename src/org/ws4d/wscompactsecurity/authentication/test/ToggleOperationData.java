package org.ws4d.wscompactsecurity.authentication.test;

import org.ws4d.wscompactsecurity.authentication.CallbackDataObject;

public class ToggleOperationData implements CallbackDataObject {

	final static long serialVersionUID = 1L;
	
	@Override
	public CallbackDataObject getObject() {
		return this;
	}
	
	String mResult;

	public String getResult() {
		return mResult;
	}

	public void setResult(String result) {
		this.mResult = result;
	}

	

}
