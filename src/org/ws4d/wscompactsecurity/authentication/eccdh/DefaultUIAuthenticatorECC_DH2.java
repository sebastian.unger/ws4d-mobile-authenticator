package org.ws4d.wscompactsecurity.authentication.eccdh;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.protocol.http.Base64Util;
import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.eccdh.DefaultECC_DH2;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;
import org.ws4d.wscompactsecurity.authentication.OperationDataCallback;
import org.ws4d.wscompactsecurity.authentication.engine.ActiveAuthentication;
import org.ws4d.wscompactsecurity.authentication.engine.IndirectAuthenticationEngine;

public class DefaultUIAuthenticatorECC_DH2 extends DefaultECC_DH2 {

	OperationDataCallback cb = null;

	public DefaultUIAuthenticatorECC_DH2() {
		this(null);
	}

	public DefaultUIAuthenticatorECC_DH2(OperationDataCallback cb) {
		super();
		this.cb = cb;
	}

	@Override
	protected ParameterValue invokeImpl(ParameterValue parameterValue,
			CredentialInfo credentialInfo) throws InvocationException,
			CommunicationException {

		String origin = ParameterValueManagement.getString(parameterValue, Constants.OnBehalfOf.getLocalPart());
		IndirectAuthenticationEngine engine = IndirectAuthenticationEngine.getInstance();
		ActiveAuthentication authentication = engine.getActiveAuthenticationByOrigin(origin);

		ParameterValue result = createOutputValue();

		authentication.setOriginNonce(ParameterValueManagement.getString(parameterValue, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_nonce.getLocalPart()));
		authentication.setOriginPublic(ParameterValueManagement.getString(parameterValue, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_public_key.getLocalPart()));
		authentication.setOldOriginCMAC(ParameterValueManagement.getString(parameterValue, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_cmac.getLocalPart()));

		if (engine.checkOriginCMac(authentication)) {
			Log.debug("Origin's CMAC passed test. Will calculate new one.");
			authentication.setNewOriginCMAC(Base64Util.encodeBytes(engine.calculateNewOriginCMac(authentication)));
		} else {
			Log.warn("Origin's CMAC failed test. Will simply forward.");
			authentication.setNewOriginCMAC(authentication.getOldOriginCMAC());
		}

		if (cb != null) {
			authentication = cb.authenticationCallback2(authentication);
		}

		if (authentication.getErrorOccured()) {
			ParameterValue fault = createFaultValue("SimpleError");
			ParameterValueManagement.setString(fault, "message", authentication.getErrorMessage());
			engine.removeActiveAuthentication(authentication);
			throw new InvocationException(getFault("SimpleError"), new QName("simple", "http://www.ws4d.org"),  fault);
		}

		if (engine.checkTargetCMac(authentication)) {
			Log.debug("Target's CMAC passed test. Will calculate new one.");
			authentication.setNewTargetCMAC(Base64Util.encodeBytes(engine.calculateNewTargetCMac(authentication)));
		} else {
			Log.warn("Target's CMAC failed test. Will simply forward.");
			authentication.setNewTargetCMAC(authentication.getOldTargetCMAC());
		}

		ParameterValueManagement.setString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.TokenType.getLocalPart(), "http://www.ws4d.org/wscompactsecurity/tokentypes#symmetric-key-token");
		ParameterValueManagement.setString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestType.getLocalPart(), "http://www.ws4d.org/AuthenticatedEllipticCurveDiffieHellman/ECC_DH2Response");
		if (authentication.isTargetBroker()) {
			ParameterValueManagement.setString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.isBroker.getLocalPart(), "true");
		} else {
			ParameterValueManagement.setString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.isBroker.getLocalPart(), "false");
		}
		if (authentication.getTargetIsAuthorizer()) {
			ParameterValueManagement.setAttributeValue(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.isBroker.getLocalPart(), Constants.authorizerAttribute, "true");
		}

		ParameterValueManagement.setString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_cmac.getLocalPart(), authentication.getNewTargetCMAC());

		ParameterValueManagement.setString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + Constants.SecurityContextToken.getLocalPart() + "/" + Constants.Identifier.getLocalPart(), authentication.getTokenIdentification());
		ParameterValueManagement.setString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + Constants.SecurityContextToken.getLocalPart() + "/" + Constants.SignWith.getLocalPart(), authentication.getTokenSignatureAlgorithm());
		ParameterValueManagement.setString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + Constants.SecurityContextToken.getLocalPart() + "/" + Constants.EncryptWith.getLocalPart(), authentication.getTokenEncryptionAlgorithm());

		return result;
	}

}
