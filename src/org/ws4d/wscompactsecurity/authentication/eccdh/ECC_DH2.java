package org.ws4d.wscompactsecurity.authentication.eccdh;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.eccdh.DefaultECC_DH2;
import org.ws4d.java.incubation.wscompactsecurity.authentication.eccdh.DefaultEndpointECC_DH2;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.ServiceAuthenticationCallbacks;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.util.Log;
import org.ws4d.wscompactsecurity.authentication.OperationDataCallback;

public class ECC_DH2 extends DefaultECC_DH2 {

	OperationDataCallback uiauth_cb = null;
	ServiceAuthenticationCallbacks endpoint_cb = null;

	public ECC_DH2() {
		this(null, null);
	}

	public ECC_DH2(OperationDataCallback uicb, ServiceAuthenticationCallbacks epcb) {
		uiauth_cb = uicb;
		endpoint_cb = epcb;
	}

	@Override
	protected ParameterValue invokeImpl(ParameterValue parameterValue,
			CredentialInfo credentialInfo) throws InvocationException,
			CommunicationException {
		String authTarget = ParameterValueManagement.getString(parameterValue, Constants.AppliesTo.getLocalPart());
		String me = AuthenticationEngine.getDefaultOwnerID();
		boolean direct = authTarget.equals(me);
		Log.debug("ECC_DH2: Comparing authTarget <" + authTarget + "> and owner uuid <" + me + "> and conclude they are " + (direct ? "identical" : "not identical"));

		DefaultECC_DH2 operation = null;

		if (direct) {
			operation = new DefaultEndpointECC_DH2(endpoint_cb);
		} else {
			operation = new DefaultUIAuthenticatorECC_DH2(uiauth_cb);
		}

		return operation.invoke(parameterValue, credentialInfo);
	}

}
