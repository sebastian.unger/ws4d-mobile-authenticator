package org.ws4d.wscompactsecurity.authentication.eccdh;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.dispatch.DeviceServiceRegistry;
import org.ws4d.java.incubation.CredentialManagement.SecurityContext;
import org.ws4d.java.incubation.CredentialManagement.SimpleContextDB;
import org.ws4d.java.incubation.Types.SecurityAlgorithmSet;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.authenticationclient.DiscoveryClient;
import org.ws4d.java.incubation.wscompactsecurity.authentication.eccdh.DefaultECC_DH1;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.securitytokenservice.DefaultSecurityTokenService;
import org.ws4d.java.incubation.wspolicy.Policy;
import org.ws4d.java.incubation.wspolicy.policymanagement.PolicyManager;
import org.ws4d.java.incubation.wspolicy.securitypolicy.AuthenticationMechanismPolicy;
import org.ws4d.java.incubation.wspolicy.wspolicyclient.WSSecurityPolicyClient;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.util.Log;
import org.ws4d.wscompactsecurity.authentication.OperationDataCallback;
import org.ws4d.wscompactsecurity.authentication.engine.ActiveAuthentication;
import org.ws4d.wscompactsecurity.authentication.engine.IndirectAuthenticationEngine;

public class DefaultUIAuthenticatorECC_DH1 extends DefaultECC_DH1 {

	OperationDataCallback cb = null;

	public DefaultUIAuthenticatorECC_DH1() {
		this(null);
	}

	public DefaultUIAuthenticatorECC_DH1(OperationDataCallback cb) {
		super();
		this.cb = cb;

	}

	@Override
	protected ParameterValue invokeImpl(ParameterValue parameterValue,
			CredentialInfo credentialInfo) throws InvocationException,
			CommunicationException {

		String authTarget = ParameterValueManagement.getString(parameterValue, Constants.AppliesTo.getLocalPart());

		/* Figure out Device Type */
		QName deviceType = null;
		QName[] deviceTypes = DiscoveryClient.getDeviceTypes(authTarget);
		QNameSet filtered = DiscoveryClient.filterDefaultDeviceTypes(deviceTypes);

		if (filtered.size() != 0)
			deviceType = (QName) filtered.iterator().next();


		DeviceReference intermediary = null;
		QName targetAuthenticationMechanism = ((DefaultSecurityTokenService)this.getService()).pickTargetAuthenticationMechanism(authTarget);

		if (targetAuthenticationMechanism != null)
			Log.info("Mechanism picked: " + targetAuthenticationMechanism);
		else {
			Log.info("NO Mechanism picked. Will check for different authenticators");
			Log.info("Will check for known authenticators");

			/* check known devices */
			SimpleContextDB db = AuthenticationEngine.getInstance().getContextDatabase();
			Iterator contexts = db.getAllContexts().values().iterator();
			while (contexts.hasNext()) {
				EndpointReference ep = new EndpointReference(new AttributedURI(((SecurityContext)contexts.next()).getContextReference()));

				Log.debug("Testing! EP        : " + ep.getAddress().toString());

				/* is device a UI Authenticator? */
				DeviceReference dr = DeviceServiceRegistry.getDeviceReference(ep, SecurityKey.EMPTY_KEY, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
				Device d = dr.getDevice();

				boolean uiAuthenticator = false;

				Iterator tit = d.getPortTypes();
				while (tit.hasNext()) {
					QName t = (QName) tit.next();
					if (t.equals(new QName(WSSecurityForDevicesConstants.UIAuthenticatorType, WSSecurityForDevicesConstants.NAMESPACE))) {
						uiAuthenticator = true;
						break;
					}
				}

				if (uiAuthenticator) {
					Log.debug("...      UI-Authenticator? YES");
				} else {
					Log.debug("...      UI-Authenticator? No. Skipping");
					continue;
				}

				Policy tempPolicy = WSSecurityPolicyClient.getAuthenticationMechanismsPolicy(ep);
				QNameSet tempMechanisms = AuthenticationMechanismPolicy.getAuthenticationMechanisms(tempPolicy);
				Log.debug("...      Policy    : " + tempPolicy.toString());
				Log.debug("...      Mechanisms: " + tempMechanisms.toString());
				Log.debug("... should contain : " + (new QName(WSSecurityForDevicesConstants.EncryptedPinExchange, WSSecurityForDevicesConstants.NAMESPACE)).toString());
				if (tempMechanisms.contains(new QName(WSSecurityForDevicesConstants.EncryptedPinExchange, WSSecurityForDevicesConstants.NAMESPACE))) {
					intermediary = DeviceServiceRegistry.getDeviceReference(ep, SecurityKey.EMPTY_KEY, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
					targetAuthenticationMechanism = new QName(WSSecurityForDevicesConstants.EncryptedPinExchange, WSSecurityForDevicesConstants.NAMESPACE);
					Log.info("Found known UI-Authenticator that supports encrypted pin exchange: " + ep.getAddress().toString());
					break;
				}
			}

			if (intermediary == null) {

				Log.info("None Found. Will probe for unknown UI-Authenticators now");

				Policy p = PolicyManager.getSecurityPolicyManagerInstance().getLocalAuthenticationPolicy().getPolicy();
				QNameSet own = AuthenticationMechanismPolicy.getAuthenticationMechanisms(p);

				intermediary = WSSecurityPolicyClient.getFirstUIAuthenticatorByMechanisms(own);

				if (intermediary == null) {
					Log.warn("Could not find intermediary. Tested Set: " + own);
					ParameterValue fault = createFaultValue("SimpleError");
					ParameterValueManagement.setString(fault, "message", "Authentication Error. Not possible to bridge UI Gap!");
					throw new InvocationException(getFault("SimpleError"), new QName("simple", "http://www.ws4d.org"),  fault);
				} else {
					QNameSet intermediaryMechanisms = AuthenticationMechanismPolicy.getAuthenticationMechanisms(WSSecurityPolicyClient.getAuthenticationMechanismsPolicy(intermediary));
					targetAuthenticationMechanism = WSSecurityPolicyClient.pickAuthenticationMechanism(own, intermediaryMechanisms);
					Log.info("Found matching UI-Authenticator: " + intermediary.getEndpointReference().getAddress().toString());
				}
			}
		}

		/* *************** */
		/* Pick algorithms */
		/* *************** */

		/* parse requested algorithms from client */
		String requestedSignatureAlgorithmsString = ParameterValueManagement.getString(parameterValue, Constants.SignWith.getLocalPart());
		String requestedEncryptionAlgorithmsString = ParameterValueManagement.getString(parameterValue, Constants.EncryptWith.getLocalPart());
		String requestedDerivationAlgorithmsString = null;

		SecurityAlgorithmSet saset = WSSecurityPolicyClient.pickSecurityAlgorithms(authTarget, requestedSignatureAlgorithmsString, requestedEncryptionAlgorithmsString, requestedDerivationAlgorithmsString);

		Log.info("Picked algorithmset: " + saset.getSignatureAlgorithm() + ", " + saset.getEncryptionAlgorithm() + ", " + saset.getDerivationAlgorithm());

		IndirectAuthenticationEngine engine = IndirectAuthenticationEngine.getInstance();

		ActiveAuthentication authentication = engine.getActiveAuthenticationByOrigin(ParameterValueManagement.getString(parameterValue, "OnBehalfOf"));

		if (authentication != null) {
			Log.debug("Found old record!");
			if (engine.removeActiveAuthenticationByOrigin(ParameterValueManagement.getString(parameterValue, Constants.OnBehalfOf.getLocalPart()))) {
				Log.debug("Could remove record. Remaining: " + engine.remainingActiveAuthentications());
			} else {
				Log.warn("Could NOT remove record. Remaining: " + engine.remainingActiveAuthentications());
			}

		}

		authentication = new ActiveAuthentication();

		authentication.setOrigin(ParameterValueManagement.getString(parameterValue, Constants.OnBehalfOf.getLocalPart()));
		authentication.setTarget(authTarget);
		authentication.setOriginMechanism(ParameterValueManagement.getString(parameterValue, Constants.AuthenticationType.getLocalPart()));
		authentication.setTargetMechanism(targetAuthenticationMechanism.toString());
		authentication.setTargetDeviceType((deviceType == null) ? "Unknown" : deviceType.getLocalPartPrefixed());
		authentication.setRequestedAlgorithmsString(requestedSignatureAlgorithmsString, requestedEncryptionAlgorithmsString, requestedDerivationAlgorithmsString);
		authentication.setSelectedSecurityAlgorithmsSet(saset);
		authentication.setIntermediary((intermediary == null) ? null : intermediary.getEndpointReference().getAddress().toString());
		if ("true".equals(ParameterValueManagement.getString(parameterValue, Constants.isBroker.getLocalPart()))) {
			authentication.setBroker(true);
			if ("true".equals(ParameterValueManagement.getAttributeValue(parameterValue, Constants.isBroker.getLocalPart(), Constants.authorizerAttribute))) {
				authentication.setIsAuthorizer(true);
			}
		}

		authentication = cb.authenticationCallback1(authentication);

		ParameterValue result = createOutputValue();

		ParameterValueManagement.setString(result, Constants.TokenType.getLocalPart(), "http://www.ws4d.org/wscompactsecurity/tokentypes#symmetric-key-token");
		ParameterValueManagement.setString(result, Constants.RequestType.getLocalPart(), "http://www.ws4d.org/AuthenticatedEllipticCurveDiffieHellman/ECC_DH1Response");
		ParameterValueManagement.setString(result, Constants.AppliesTo.getLocalPart(), authentication.getOrigin());
		ParameterValueManagement.setString(result, Constants.OnBehalfOf.getLocalPart(), authentication.getTarget());
		ParameterValueManagement.setString(result, Constants.AuthenticationType.getLocalPart(), authentication.getOriginMechanism());

		ParameterValueManagement.setString(result, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_curvename.getLocalPart(), authentication.getCurveName());
		ParameterValueManagement.setString(result, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_nonce.getLocalPart(), authentication.getTargetNonce());
		ParameterValueManagement.setString(result, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_public_key.getLocalPart(), authentication.getTargetPublic());

		if (authentication.getOriginMechanism().equals(WSSecurityForDevicesConstants.EncryptedPinExchangeAsQNameString)) {
			// TODO AND IF REQUEST IS ENCRYPTED
			ParameterValueManagement.setString(result, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_numerical_pin.getLocalPart(), Integer.toString(authentication.getOOBSharedSecret()));
		}

		engine.addActiveAuthentication(authentication);
		return result;
	}

}
