package org.ws4d.wscompactsecurity.authentication.engine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;

import org.ws4d.java.incubation.Types.SecurityAlgorithmSet;
import org.ws4d.java.types.QName;

public class ActiveAuthentication implements Serializable {

	private static final long serialVersionUID = 1L;

	private String mOrigin;
	private String mOriginMechanism;

	private String mTarget;
	private String mTargetMechanism;
	private String mTargetDeviceType;

	private Boolean mErrorOccured = false;
	private String mErrorMessage = null;

	private int mCurrentStage = 0;

	private int mOOBSharedSecret = -1;

	private String mNonceOrigin;
	private String mNonceTarget;
	private String mCurveName;
	private String mOriginPublic;
	private String mTargetPublic;

	private String mOldTargetCMAC;
	private String mOldOriginCMAC;
	private String mNewTargetCMAC;
	private String mNewOriginCMAC;

	HashSet<String> mTargetOther;
	HashSet<String> mOriginOther;

	private ArrayList<String> requestedSignatureAlgorithms;
	private ArrayList<String> requestedEncryptionAlgorithms;
	private ArrayList<String> requestedDerivationAlgorithms;
	private SecurityAlgorithmSet saset;

	boolean isBroker;
	boolean isAuthorizer;
	boolean targetIsBroker;
	boolean targetIsAuthorizer;

	private String tokenIdentification;
	private String tokenSigAlg;
	private String tokenEncAlg;

	private String requestedDerivationAlgorithmsString;
	private String requestedEncryptionAlgorithmsString;
	private String requestedSignatureAlgorithmsString;

	public ActiveAuthentication() {
		mOrigin = null;
		mOriginMechanism = null;

		mTarget = null;
		mTargetMechanism = null;
		mTargetDeviceType = null;

		mErrorOccured = false;
		mErrorMessage = null;

		mCurrentStage = 0;

		mOOBSharedSecret = -1;

		mNonceOrigin = null;
		mNonceTarget = null;
		mCurveName = null;
		mOriginPublic = null;
		mTargetPublic = null;

		mOldTargetCMAC = null;
		mOldOriginCMAC = null;
		mNewTargetCMAC = null;
		mNewOriginCMAC = null;

		mTargetOther = new HashSet<String>();
		mTargetOther.clear();
		mOriginOther = new HashSet<String>();
		mOriginOther.clear();

		requestedSignatureAlgorithms = null;
		requestedEncryptionAlgorithms = null;
		requestedDerivationAlgorithms = null;
		saset = null;

		isBroker = false;
		targetIsBroker = false;
	}

	public String getOrigin() {
		return mOrigin;
	}
	public void setOrigin(String origin) {
		this.mOrigin = origin;
	}
	public String getOriginMechanism() {
		return mOriginMechanism;
	}
	public void setOriginMechanism(String originMechanism) {
		this.mOriginMechanism = originMechanism;
	}
	public String getTarget() {
		return mTarget;
	}
	public void setTarget(String target) {
		this.mTarget = target;
	}
	public String getTargetMechanism() {
		return mTargetMechanism;
	}
	public void setTargetMechanism(String targetMechanism) {
		this.mTargetMechanism = targetMechanism;
	}
	public String getTargetDeviceType() {
		return mTargetDeviceType;
	}
	public void setTargetDeviceType(String targetDeviceType) {
		this.mTargetDeviceType = targetDeviceType;
	}
	public Boolean getErrorOccured() {
		return mErrorOccured;
	}
	public void setErrorOccured(Boolean errorOccured) {
		this.mErrorOccured = errorOccured;
	}
	public String getErrorMessage() {
		return mErrorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.mErrorMessage = errorMessage;
	}
	public int getCurrentStage() {
		return mCurrentStage;
	}
	public void setCurrentStage(int mCurrentStage) {
		this.mCurrentStage = mCurrentStage;
	}
	public int getOOBSharedSecret() {
		return mOOBSharedSecret;
	}
	public Boolean[] getOOBSharedSecretAsBinary () {
		ArrayList<Boolean> key = new ArrayList<Boolean>();
		String s = Integer.toBinaryString(mOOBSharedSecret);
		for (int i = 0; i < s.length(); i++) {
			key.add(s.charAt(i) == '1');
		}
		return key.toArray(new Boolean[key.size()]);
	}
	public byte[] getOOBSharedSecretAsByteArray () {
		byte[] key = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		key[15] = (byte) ((getOOBSharedSecret()) & 0x00FF);
		key[14] = (byte) ((getOOBSharedSecret() >> 8) & 0x00FF);
		key[13] = (byte) ((getOOBSharedSecret() >> 16) & 0x00FF);
		return key;
	}
	public void setOOBSharedSecret(int mOOBSharedSecret) {
		this.mOOBSharedSecret = mOOBSharedSecret;
	}
	public String getOriginNonce() {
		return mNonceOrigin;
	}
	public void setOriginNonce(String nonce) {
		this.mNonceOrigin = nonce;
	}
	public String getCurveName() {
		return mCurveName;
	}
	public void setCurveName(String curveName) {
		this.mCurveName = curveName;
	}
	public String getOriginPublic() {
		return mOriginPublic;
	}
	public void setOriginPublic(String publicKey) {
		this.mOriginPublic = publicKey;
	}
	public String getOldTargetCMAC() {
		return mOldTargetCMAC;
	}
	public void setOldTargetCMAC(String oldTargetCMAC) {
		this.mOldTargetCMAC = oldTargetCMAC;
	}
	public String getOldOriginCMAC() {
		return mOldOriginCMAC;
	}
	public void setOldOriginCMAC(String oldOriginCMAC) {
		this.mOldOriginCMAC = oldOriginCMAC;
	}
	public String getNewTargetCMAC() {
		return mNewTargetCMAC;
	}
	public void setNewTargetCMAC(String newTargetCMAC) {
		this.mNewTargetCMAC = newTargetCMAC;
	}
	public String getNewOriginCMAC() {
		return mNewOriginCMAC;
	}
	public void setNewOriginCMAC(String newOriginCMAC) {
		this.mNewOriginCMAC = newOriginCMAC;
	}
	public String getTargetNonce() {
		return mNonceTarget;
	}
	public void setTargetNonce(String nonceB) {
		this.mNonceTarget = nonceB;
	}
	public String getTargetPublic() {
		return mTargetPublic;
	}
	public void setTargetPublic(String mTargetPublic) {
		this.mTargetPublic = mTargetPublic;
	}
	public HashSet<String> getTargetOther() {
		return mTargetOther;
	}
	public HashSet<String> getOriginOther() {
		return mOriginOther;
	}

	public org.ws4d.java.structures.ArrayList getRequestedSignatureAlgorithms() {
		return new org.ws4d.java.structures.ArrayList(this.requestedSignatureAlgorithms.toArray());
	}

	public void setRequestedSignatureAlgorithms(
			org.ws4d.java.structures.ArrayList requestedSignatureAlgorithms) {
		if (this.requestedSignatureAlgorithms == null) {
			this.requestedSignatureAlgorithms = new ArrayList<String>();
		}
		for (int i = 0; i < requestedSignatureAlgorithms.size(); i++) {
			this.requestedSignatureAlgorithms.add(((QName)requestedSignatureAlgorithms.get(i)).toString());
		}
	}

	public org.ws4d.java.structures.ArrayList getRequestedEncryptionAlgorithms() {
		return new org.ws4d.java.structures.ArrayList(this.requestedEncryptionAlgorithms.toArray());
	}

	public void setRequestedEncryptionAlgorithms(
			org.ws4d.java.structures.ArrayList requestedEncryptionAlgorithms) {
		if (this.requestedEncryptionAlgorithms == null) {
			this.requestedEncryptionAlgorithms = new ArrayList<String>();
		}
		for (int i = 0; i < requestedEncryptionAlgorithms.size(); i++) {
			this.requestedEncryptionAlgorithms.add(((QName)requestedEncryptionAlgorithms.get(i)).toString());
		}
	}

	public org.ws4d.java.structures.ArrayList getRequestedDerivationAlgorithms() {
		return new org.ws4d.java.structures.ArrayList(requestedDerivationAlgorithms.toArray());
	}

	public void setRequestedDerivationAlgorithms(
			org.ws4d.java.structures.ArrayList requestedDerivationAlgorithms) {
		if (this.requestedDerivationAlgorithms == null) {
			this.requestedDerivationAlgorithms = new ArrayList<String>();
		}
		for (int i = 0; i < requestedDerivationAlgorithms.size(); i++) {
			this.requestedDerivationAlgorithms.add(((QName)requestedDerivationAlgorithms.get(i)).toString());
		}
	}

	public SecurityAlgorithmSet getSelectedSecurityAlgorithmsSet() {
		return saset;
	}

	public void setSelectedSecurityAlgorithmsSet(SecurityAlgorithmSet saset) {
		this.saset = saset;
	}

	public String getTokenIdentification() {
		return tokenIdentification;
	}

	public void setTokenIdentification(String tokenIdentification) {
		this.tokenIdentification = tokenIdentification;
	}

	public String getTokenSignatureAlgorithm() {
		return tokenSigAlg;
	}

	public void setTokenSignatureAlgorithm(String tokenSigAlg) {
		this.tokenSigAlg = tokenSigAlg;
	}

	public String getTokenEncryptionAlgorithm() {
		return tokenEncAlg;
	}

	public void setTokenEncryptionAlgorithm(String tokenEncAlg) {
		this.tokenEncAlg = tokenEncAlg;
	}

	public void setRequestedAlgorithmsString(
			String requestedSignatureAlgorithmsString,
			String requestedEncryptionAlgorithmsString,
			String requestedDerivationAlgorithmsString) {
		this.requestedSignatureAlgorithmsString = requestedSignatureAlgorithmsString;
		this.requestedEncryptionAlgorithmsString = requestedEncryptionAlgorithmsString;
		this.requestedDerivationAlgorithmsString = requestedDerivationAlgorithmsString;

		String[] sigs = requestedSignatureAlgorithmsString.split(",");
		String[] encs = requestedEncryptionAlgorithmsString.split(",");
		//		String[] kdfs =
		org.ws4d.java.structures.ArrayList qsigs = new org.ws4d.java.structures.ArrayList();
		org.ws4d.java.structures.ArrayList qencs = new org.ws4d.java.structures.ArrayList();

		for (int i = 0 ; i < sigs.length ; i++) {
			qsigs.add(QName.construct(sigs[i]));
		}
		for (int i = 0 ; i < encs.length ; i++) {
			qencs.add(QName.construct(encs[i]));
		}

		setRequestedSignatureAlgorithms(qsigs);
		setRequestedEncryptionAlgorithms(qencs);
		//		setRequestedDerivationAlgorithms(new org.ws4d.java.structures.ArrayList(requestedDerivationAlgorithmsString.split(",")));

	}

	public String getRequestedDerivationAlgorithmsString() {
		return requestedDerivationAlgorithmsString;
	}

	public String getRequestedEncryptionAlgorithmsString() {
		return requestedEncryptionAlgorithmsString;
	}

	public String getRequestedSignatureAlgorithmsString() {
		return requestedSignatureAlgorithmsString;
	}

	public boolean isBroker() {
		return isBroker;
	}

	public void setBroker(boolean isBroker) {
		this.isBroker = isBroker;
	}

	public void setTargetIsBroker(boolean is) {
		targetIsBroker = is;
	}

	public boolean isTargetBroker() {
		return targetIsBroker;
	}

	String intermediary = null;
	public void setIntermediary(String address) {
		intermediary = address;
	}
	public String getIntermediary() {
		return intermediary;
	}

	public void setIsAuthorizer(boolean b) {
		this.isAuthorizer = b;
	}
	public boolean getIsAuthorizer() {
		return this.isAuthorizer;
	}
	public void setTargetIsAuthorizer(boolean b) {
		this.targetIsAuthorizer = b;
	}
	public boolean getTargetIsAuthorizer() {
		return this.targetIsAuthorizer;
	}
}
