package org.ws4d.wscompactsecurity.authentication.engine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

import org.spongycastle.crypto.engines.AESFastEngine;
import org.spongycastle.crypto.macs.CMac;
import org.spongycastle.crypto.params.KeyParameter;
import org.ws4d.java.communication.protocol.http.Base64Util;
import org.ws4d.java.incubation.Types.SecurityAlgorithmSet;

import android.util.Log;

public class IndirectAuthenticationEngine {

	private static IndirectAuthenticationEngine instance = null;

	public static IndirectAuthenticationEngine getInstance() {
		if (instance == null) {
			instance = new IndirectAuthenticationEngine();
		}
		return instance;
	}

	private Vector<ActiveAuthentication> openAuthentications = null;

	public IndirectAuthenticationEngine() {
		openAuthentications = new Vector<ActiveAuthentication>();
	}

	public void addActiveAuthentication(ActiveAuthentication aa) {
		openAuthentications.add(aa);
	}

	public boolean removeActiveAuthenticationByOrigin(String originId) {
		for (ActiveAuthentication aa : openAuthentications) {
			if (aa.getOrigin().equals(originId)) {
				return openAuthentications.remove(aa);
			}
		}
		return false;
	}

	public boolean removeActiveAuthentication(ActiveAuthentication aa) {
		return openAuthentications.remove(aa);
	}

	public int remainingActiveAuthentications() {
		return openAuthentications.size();
	}

	public ActiveAuthentication getActiveAuthenticationByOrigin (String originId) {
		for (ActiveAuthentication aa : openAuthentications) {
			if (aa.getOrigin().equals(originId)) {
				return aa;
			}
		}
		return null;
	}

	public boolean checkOriginCMac (ActiveAuthentication aa) {

		CMac cmac = new CMac(new AESFastEngine(), 64);

		String sKey = "";

		for (int i = 0; i < aa.getOOBSharedSecretAsByteArray().length; i++) {
			sKey += Integer.toHexString(aa.getOOBSharedSecretAsByteArray()[i]) + " ";
		}

		Log.d("CheckOriginMac", "Key: " + sKey);


		cmac.init(new KeyParameter(aa.getOOBSharedSecretAsByteArray()));

		cmac.update(aa.getOrigin().getBytes(), 0, aa.getOrigin().getBytes().length);
		Log.d("CheckOriginMac", "Origin: " + aa.getOrigin());
		cmac.update(aa.getTarget().getBytes(), 0, aa.getTarget().getBytes().length);
		Log.d("CheckOriginMac", "Target: " + aa.getTarget());

		cmac.update(Base64Util.decode(aa.getOriginNonce()), 0, Base64Util.decode(aa.getOriginNonce()).length);
		Log.d("CheckOriginMac", "Origin Nonce: " + aa.getOriginNonce());
		cmac.update(Base64Util.decode(aa.getTargetNonce()), 0, Base64Util.decode(aa.getTargetNonce()).length);
		Log.d("CheckOriginMac", "Target Nonce: " + aa.getTargetNonce());

		aa.getOriginOther().add(aa.getCurveName());
		aa.getOriginOther().add(aa.getOriginMechanism());
		if (aa.getRequestedSignatureAlgorithmsString() != null)
			aa.getOriginOther().add(aa.getRequestedSignatureAlgorithmsString());
		if (aa.getRequestedEncryptionAlgorithmsString() != null)
			aa.getOriginOther().add(aa.getRequestedEncryptionAlgorithmsString());
		if (aa.getRequestedDerivationAlgorithmsString() != null)
			aa.getOriginOther().add(aa.getRequestedDerivationAlgorithmsString());

		ArrayList<String> sortedOther = new ArrayList<String>(aa.getOriginOther());

		Collections.sort(sortedOther);

		String oString = "";

		for (String s  : sortedOther) {
			oString += s;
		}

		cmac.update(oString.getBytes(), 0, oString.getBytes().length);
		Log.d("CheckOriginMac", "Other: " + oString);

		byte[] tmpMac = new byte[8];

		cmac.doFinal(tmpMac, 0);

		Log.d("CheckOriginMac", "Calculated: " + Base64Util.encodeBytes(tmpMac));
		Log.d("CheckOriginMac", "Received: " + aa.getOldOriginCMAC());

		byte[] origMac = Base64Util.decode(aa.getOldOriginCMAC());

		for (int i = 0; i < 8; i++) {
			if (origMac[i] != tmpMac[i])
				return false;
		}

		return true;
	}

	public byte[] calculateNewOriginCMac (ActiveAuthentication aa) {

		byte[] out = new byte[8];

		CMac cmac = new CMac(new AESFastEngine(), 64);

		cmac.init(new KeyParameter(aa.getOOBSharedSecretAsByteArray()));

		cmac.update(aa.getOrigin().getBytes(), 0, aa.getOrigin().getBytes().length);
		Log.d("calculateNewOriginCMac", "Origin: " + aa.getOrigin());
		cmac.update(aa.getTarget().getBytes(), 0, aa.getTarget().getBytes().length);
		Log.d("calculateNewOriginCMac", "Target: " + aa.getTarget());

		cmac.update(Base64Util.decode(aa.getOriginNonce()), 0, Base64Util.decode(aa.getOriginNonce()).length);
		Log.d("calculateNewOriginCMac", "Origin Nonce: " + aa.getOriginNonce());
		cmac.update(Base64Util.decode(aa.getTargetNonce()), 0, Base64Util.decode(aa.getTargetNonce()).length);
		Log.d("calculateNewOriginCMac", "Target Nonce: " + aa.getTargetNonce());

		ArrayList<String> other = new ArrayList<String>();

		other.add(aa.getCurveName());

		other.add(aa.getTargetMechanism()); /* Aye, there's the rub! */

		SecurityAlgorithmSet saset = aa.getSelectedSecurityAlgorithmsSet();

		if (saset.getSignatureAlgorithm() != null)
			other.add(saset.getSignatureAlgorithm().toString());
		if (saset.getEncryptionAlgorithm() != null)
			other.add(saset.getEncryptionAlgorithm().toString());
		if (saset.getDerivationAlgorithm() != null)
			other.add(saset.getDerivationAlgorithm().toString());

		Collections.sort(other);

		String oString = "";

		for (String s  : other) {
			oString += s;
		}

		Log.d("calculateNewOriginCMac", "Other: " + oString);

		cmac.update(oString.getBytes(), 0, oString.getBytes().length);

		cmac.doFinal(out, 0);

		return out;
	}

	public boolean checkTargetCMac (ActiveAuthentication aa) {

		CMac cmac = new CMac(new AESFastEngine(), 64);

		cmac.init(new KeyParameter(aa.getOOBSharedSecretAsByteArray()));

		cmac.update(aa.getTarget().getBytes(), 0, aa.getTarget().getBytes().length);
		Log.d("CheckTargetMac", "Origin: " + aa.getTarget());
		cmac.update(aa.getOrigin().getBytes(), 0, aa.getOrigin().getBytes().length);
		Log.d("CheckTargetMac", "Target: " + aa.getOrigin());

		cmac.update(Base64Util.decode(aa.getTargetNonce()), 0, Base64Util.decode(aa.getTargetNonce()).length);
		Log.d("CheckTargetMac", "Origin Nonce: " + aa.getTargetNonce());
		cmac.update(Base64Util.decode(aa.getOriginNonce()), 0, Base64Util.decode(aa.getOriginNonce()).length);
		Log.d("CheckTargetMac", "Target Nonce: " + aa.getOriginNonce());

		aa.getTargetOther().add(aa.getCurveName());

		aa.getTargetOther().add(aa.getTokenIdentification());
		aa.getTargetOther().add(aa.getTokenSignatureAlgorithm());
		aa.getTargetOther().add(aa.getTokenEncryptionAlgorithm());

		ArrayList<String> sortedOther = new ArrayList<String>();
		sortedOther.addAll(aa.getTargetOther());

		Collections.sort(sortedOther);

		String oString = "";

		for (String s  : sortedOther) {
			oString += s;
		}

		Log.d("CheckTargetMac", "Other: " + oString);

		cmac.update(oString.getBytes(), 0, oString.getBytes().length);

		byte[] tmpMac = new byte[8];

		cmac.doFinal(tmpMac, 0);

		byte[] origMac = Base64Util.decode(aa.getOldTargetCMAC());

		for (int i = 0; i < 8; i++) {
			if (origMac[i] != tmpMac[i])
				return false;
		}

		return true;
	}

	public byte[] calculateNewTargetCMac (ActiveAuthentication aa) {

		byte[] out = new byte[8];

		CMac cmac = new CMac(new AESFastEngine(), 64);

		cmac.init(new KeyParameter(aa.getOOBSharedSecretAsByteArray()));

		cmac.update(aa.getTarget().getBytes(), 0, aa.getTarget().getBytes().length);
		Log.d("calculateNewTargetCMac", "Origin: " + aa.getTarget());
		cmac.update(aa.getOrigin().getBytes(), 0, aa.getOrigin().getBytes().length);
		Log.d("calculateNewTargetCMac", "Target: " + aa.getOrigin());

		cmac.update(Base64Util.decode(aa.getTargetNonce()), 0, Base64Util.decode(aa.getTargetNonce()).length);
		Log.d("calculateNewTargetCMac", "Origin Nonce: " + aa.getTargetNonce());
		cmac.update(Base64Util.decode(aa.getOriginNonce()), 0, Base64Util.decode(aa.getOriginNonce()).length);
		Log.d("calculateNewTargetCMac", "Target Nonce: " + aa.getOriginNonce());

		ArrayList<String> other = new ArrayList<String>();

		other.add(aa.getCurveName());

		other.add(aa.getOriginMechanism()); /* Aye, there's the rub! */

		other.add(aa.getTokenIdentification());
		other.add(aa.getTokenSignatureAlgorithm());
		other.add(aa.getTokenEncryptionAlgorithm());

		Collections.sort(other);

		String oString = "";

		for (String s  : other) {
			oString += s;
		}

		Log.d("calculateNewTargetCMac", "Other: " + oString);

		cmac.update(oString.getBytes(), 0, oString.getBytes().length);

		cmac.doFinal(out, 0);

		return out;
	}

}
