#!/bin/bash

ADBPATH=/home/su009/Software/android-sdk-linux/platform-tools

ADB=adb

devicenumplusone=`${ADBPATH}/${ADB} devices | cut -f 1 | wc -l `

let devicenum=$devicenumplusone-1

devices=`${ADBPATH}/${ADB} devices | cut -f 1 | tail -n $devicenum`

for device in $devices
do
	echo "Killing App on device $device..."
	${ADBPATH}/${ADB} -s $device shell am force-stop org.ws4d.mobile.authenticator 
done
